package com.yoonji.snow.videoedit;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

/**
 * Created by snow on 2017-08-19.
 */

public class StartPage extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start_page);

        Handler handler = new Handler() {
            @Override
            public void handleMessage(Message message) {
                finish();
            }
        };
        handler.sendEmptyMessageDelayed(0,2000);
    }
}

