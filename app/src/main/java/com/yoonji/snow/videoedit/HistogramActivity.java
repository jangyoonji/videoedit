package com.yoonji.snow.videoedit;

import android.app.Activity;
import android.os.Bundle;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

/**
 * Created by snow on 2017-08-11.
 */

public class HistogramActivity extends Activity {
    LineGraphSeries<DataPoint> series_H;
    LineGraphSeries<DataPoint> series_S;
    LineGraphSeries<DataPoint> series_V;

    int hsv_histogram_H[];
    int hsv_histogram_S[];
    int hsv_histogram_V[];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.histogram_view);

        hsv_histogram_H = new int[361];
        hsv_histogram_S = new int[101];
        hsv_histogram_V = new int[256];

        Bundle bundle = getIntent().getExtras();
        hsv_histogram_H = bundle.getIntArray("histogram_H");
        hsv_histogram_S = bundle.getIntArray("histogram_S");
        hsv_histogram_V = bundle.getIntArray("histogram_V");

        GraphView graphView_H = (GraphView) findViewById(R.id.graph_H);
        GraphView graphView_S = (GraphView) findViewById(R.id.graph_S);
        GraphView graphView_V = (GraphView) findViewById(R.id.graph_V);

        series_H = new LineGraphSeries<DataPoint>();
        series_S = new LineGraphSeries<DataPoint>();
        series_V = new LineGraphSeries<DataPoint>();

        for(int i=0; i<hsv_histogram_H.length; i++) {
            series_H.appendData(new DataPoint(i,hsv_histogram_H[i]),true,hsv_histogram_H.length);
        }
        for(int i=0; i<hsv_histogram_S.length; i++) {
            series_S.appendData(new DataPoint(i,hsv_histogram_S[i]),true,hsv_histogram_S.length);
        }
        for(int i=0; i<hsv_histogram_V.length; i++) {
            series_V.appendData(new DataPoint(i,hsv_histogram_V[i]),true,hsv_histogram_V.length);
        }

        graphView_H.addSeries(series_H);
        graphView_S.addSeries(series_S);
        graphView_V.addSeries(series_V);



    }
}
