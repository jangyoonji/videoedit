package com.yoonji.snow.videoedit;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.VideoView;

/**
 * Created by snow on 2017-08-08.
 */

public class MseVideoView extends VideoView {
    public MseVideoView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(widthMeasureSpec, heightMeasureSpec);
    }

}
