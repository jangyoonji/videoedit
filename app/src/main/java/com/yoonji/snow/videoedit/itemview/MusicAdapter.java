package com.yoonji.snow.videoedit.itemview;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.yoonji.snow.videoedit.R;

import java.util.List;

/**
 * Created by snow on 2017-08-09.
 */

public class MusicAdapter extends RecyclerView.Adapter<MusicAdapter.ViewHolder> {

    private final String TAG = "MusicAdapter";
    private List<MusicItem> mDataset;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView mImageView;
        public TextView mTextView;
        public ViewHolder(View itemView) {
            super(itemView);
            mImageView = (ImageView) itemView.findViewById(R.id.image);
            mTextView = (TextView)  itemView.findViewById(R.id.title);
            mImageView.setScaleType(ImageView.ScaleType.FIT_XY);
        }
    }

    public MusicAdapter(List<MusicItem> myDataset) {
        mDataset = myDataset;
    }

    @Override
    public MusicAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.musicitem_view,parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        Log.d(TAG,"onCreateViewHolder");
        return viewHolder;
    }
    @Override
    public void onBindViewHolder(MusicAdapter.ViewHolder holder, int position) {
        //holder.mImageView.setImageDrawable(mDataset.get(position).getIcon());

        holder.mImageView.setImageDrawable(mDataset.get(position).getDrawable());
        holder.mTextView.setText(mDataset.get(position).getTitle());

        if (mDataset.get(position).getIsPicked() == true) {
            Log.d(TAG,"change positon "+position+" to pink");
            holder.mImageView.setBackgroundColor(Color.parseColor("#FF1493"));
        }
        else {
            Log.d(TAG,"change positon "+position+" to white");
            holder.mImageView.setBackgroundColor(Color.parseColor("#FFFFFF"));
        }
        Log.d("musicAdapter","onBindViewHolder position: "+position);


    }


    @Override
    public void onViewRecycled(MusicAdapter.ViewHolder holder) {
        super.onViewRecycled(holder);
    }

    @Override
    public int getItemCount() { return mDataset.size(); }
}
