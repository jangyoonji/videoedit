package com.yoonji.snow.videoedit.itemview;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.yoonji.snow.videoedit.R;

import java.io.File;
import java.util.List;

/**
 * Created by snow on 2017-07-24.
 */
@RequiresApi(api = Build.VERSION_CODES.M)

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
    private List<TimelineItem> mDataset;
    private ViewGroup viewGroup;
    //private Handler handler;
    private int ThumbnailWidth=0;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView mImageView;
        public ImageView mVideoButton;
        public ImageView mSelectView;
        public TimelineItem timelineItem;


        public ViewHolder(View itemView) {
            super(itemView);
            mImageView = (ImageView) itemView.findViewById(R.id.image);
            mVideoButton = (ImageView) itemView.findViewById(R.id.playButton);
            //mSelectView = (ImageView) itemView.findViewById(R.id.selectView);
            mImageView.setScaleType(ImageView.ScaleType.FIT_XY);
            mVideoButton.setScaleType(ImageView.ScaleType.FIT_XY);

        }
    }


    public MyAdapter(List<TimelineItem> myDataset) {
        mDataset = myDataset;
    }

    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.timeline_view,parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        int imageViewWidth = (int)(parent.getWidth()/4.5f);
        ThumbnailWidth = parent.getWidth()/8;
        viewGroup=parent;



        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) viewHolder.mImageView.getLayoutParams();
        params.width = imageViewWidth;
        params.height = imageViewWidth;
        viewHolder.mImageView.setLayoutParams(params);



        FrameLayout.LayoutParams params1 = (FrameLayout.LayoutParams) viewHolder.mVideoButton.getLayoutParams();
        params1.width = imageViewWidth/3;
        params1.height = imageViewWidth/3;

        viewHolder.mVideoButton.setLayoutParams(params1);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.timelineItem = mDataset.get(position);

        if(position >= mDataset.size()) return;

        Glide.with(viewGroup.getContext())
                .load(new File(holder.timelineItem.getOriginalPath()))
                .thumbnail(0.1f)
                .into(holder.mImageView);

        if(holder.timelineItem.getIsHighlight() == true)
            holder.mImageView.setBackground(viewGroup.getContext().getDrawable(R.drawable.highlight_image));
        else if (holder.timelineItem.getIsPicked() == true)
            holder.mImageView.setBackground(viewGroup.getContext().getDrawable(R.drawable.select_image));
        else
            holder.mImageView.setBackgroundColor(Color.parseColor("#000000"));

        if(holder.timelineItem.getIsImage())
            holder.mVideoButton.setVisibility(View.INVISIBLE);
        else
            holder.mVideoButton.setVisibility(View.VISIBLE);
    }


    @Override
    public void onViewRecycled(ViewHolder holder) {
        super.onViewRecycled(holder);
    }


    @Override
    public int getItemCount() { return mDataset.size(); }


}
