package com.yoonji.snow.videoedit.itemview;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Parcel;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.util.Log;

import java.util.Arrays;

/**
 * Created by snow on 2017-07-24.
 */

public class TimelineItem implements Parcelable {

    private long mId;
    private String mFilePath;
    public int mWidth;
    public int mHeight;
    private long dateTaken;
    private long duration;

    private boolean isImage;
    private boolean isPicked;
    private boolean thumbnailUpdated;
    private boolean hasHistogram;
    private boolean isSimilarImage;
    private boolean isError;
    private boolean isHighlight;


    private int orientation;
    private boolean isSelected;



    public TimelineItem() {
        mId = 0;
        mFilePath = null;
        mWidth= 0;
        mHeight= 0;
        dateTaken = 0;
        duration = 2;

        isImage = false;
        isPicked = true;
        thumbnailUpdated = false;
        hasHistogram = false;
        isSimilarImage=false;
        isError =false;
        orientation = 0;
        isHighlight = false;

        isSelected = false;
    }

    public TimelineItem(Parcel in) {
        readFromParcel(in);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(mId);
        dest.writeString(mFilePath);
        dest.writeInt(mWidth);
        dest.writeInt(mHeight);
        dest.writeLong(dateTaken);
        dest.writeLong(duration);
        dest.writeString(Boolean.toString(isImage));
        dest.writeString(Boolean.toString(hasHistogram));
        dest.writeString(Boolean.toString(isSimilarImage));
        dest.writeString(Boolean.toString(isError));
        dest.writeInt(orientation);
        dest.writeString(Boolean.toString(isHighlight));
    }

    public void readFromParcel(Parcel in) {
        mId = in.readLong();
        mFilePath = in.readString();
        mWidth= in.readInt();
        mHeight= in.readInt();
        dateTaken = in.readLong();
        duration = in.readLong();

        isImage = Boolean.valueOf(in.readString());
        hasHistogram = Boolean.valueOf(in.readString());
        isSimilarImage = Boolean.valueOf(in.readString());
        isError = Boolean.valueOf(in.readString());
        orientation = in.readInt();
        isHighlight = Boolean.valueOf(in.readString());
    }

    public static final Parcelable.Creator CREATOR =
            new Parcelable.Creator() {
                public TimelineItem createFromParcel(Parcel in) {
                    return new TimelineItem(in);
                }

                public TimelineItem[] newArray(int size) {
                    return new TimelineItem[size];
                }
            };

    public void init(Cursor cursor) {
        mId = cursor.getLong(cursor.getColumnIndex(MediaStore.Files.FileColumns._ID));
        mFilePath = cursor.getString(cursor.getColumnIndex(MediaStore.Video.Media.DATA));
        mWidth =cursor.getInt(cursor.getColumnIndex(MediaStore.Images.Media.WIDTH));
        mHeight =cursor.getInt(cursor.getColumnIndex(MediaStore.Images.Media.HEIGHT));
        dateTaken = cursor.getInt(cursor.getColumnIndex(MediaStore.Images.Media.DATE_TAKEN));
        duration = cursor.getLong(cursor.getColumnIndex(MediaStore.Video.Media.DURATION));
        orientation = cursor.getInt(cursor.getColumnIndex(MediaStore.Images.Media.ORIENTATION));

        isSelected = false;



        if(cursor.getInt(cursor.getColumnIndex(MediaStore.Files.FileColumns.MEDIA_TYPE))
                == MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE)
        {
            isImage = true;
            duration=2000;
            //Log.d("timelineItem", "isImage is true");


        }
        else {
            isImage = false;
            /*
            Log.d("timelineItem","media type: "+ cursor.getInt(cursor.getColumnIndex(MediaStore.Files.FileColumns.MEDIA_TYPE))
                    + "media type image: "+ MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE);
            Log.d("timelineItem", "isImage is false");
            */
            duration=2000;
        }
        isError = false;

    }


    public void setIsSelected(boolean tf) { isSelected = tf;}
    public boolean getIsSelected() { return isSelected; }
    public void setIsPicked(boolean tf) { isPicked = tf; }
    public boolean getIsPicked() { return isPicked; }

    public boolean getIsImage() {
        return isImage;
    }

    public String getOriginalPath() { return mFilePath; }

    public void setIsHighlight(boolean tf) { isHighlight = tf; }
    public boolean getIsHighlight() { return isHighlight;}

    public long getId() { return mId; }
    public long getDuration() { return duration; }
    public void setDuration(long duration1) { duration = duration1;}


    public boolean getIsSimilarImage() { return isSimilarImage; }

    public void setIsSimilarImage(boolean tf) { isSimilarImage = tf;}

    public boolean getIsError() { return isError;}

    public int getmWidth() { return mWidth; }
    public int getmHeight() { return mHeight; }

    public int getOrientation() { return orientation; }




}
