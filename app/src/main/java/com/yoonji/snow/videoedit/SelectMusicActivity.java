package com.yoonji.snow.videoedit;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.yoonji.snow.videoedit.itemview.MusicAdapter;
import com.yoonji.snow.videoedit.itemview.MusicItem;

import java.util.ArrayList;

/**
 * Created by snow on 2017-08-09.
 */

public class SelectMusicActivity extends Activity {

    private final String TAG = "SelectMusicActivity";
    static final int PICK_MUSIC_REQUEST = 1;

    private ArrayList<MusicItem> myMusicSet;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private ImageButton okButton;

    private MusicItem pickedItem;
    private int pickedPosition;

    private MediaPlayer mediaPlayer;
    private int length;
    boolean isMusicPlaying;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selectmusic);
        recyclerView = (RecyclerView) findViewById(R.id.musicView);
        recyclerView.setHasFixedSize(true);
        okButton = (ImageButton) findViewById(R.id.okButton);

        mLayoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(mLayoutManager);

        myMusicSet = new ArrayList<MusicItem>();
        mAdapter = new MusicAdapter(myMusicSet);
        recyclerView.setAdapter(mAdapter);


        myMusicSet.add(new MusicItem(0, "atlanta", R.raw.atlanta, getResources().getDrawable(R.drawable.atlanta_thumbnail)));
        myMusicSet.add(new MusicItem(0, "humidity", R.raw.humidity, getResources().getDrawable(R.drawable.humidity_thumbnail)));
        myMusicSet.add(new MusicItem(0, "splashing_around", R.raw.splashing_around, getResources().getDrawable(R.drawable.splashing_around_thumbnail)));


        pickedPosition = -1;

        pickedItem = null;
        isMusicPlaying = false;
        length = 0;

        mAdapter.notifyDataSetChanged();


        final GestureDetector gestureDetector = new GestureDetector(this,new GestureDetector.SimpleOnGestureListener()
        {
            @Override
            public boolean onSingleTapUp(MotionEvent e)
            {
                return true;
            }
        });
        recyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                Log.d(TAG,"onInterceptTouchEvent");
                View child = rv.findChildViewUnder(e.getX(), e.getY());
                if(child!=null&&gestureDetector.onTouchEvent(e)) {

                    if(rv.getChildAdapterPosition(child) != pickedPosition) {
                        int position = rv.getChildAdapterPosition(child);
                        pickItem(position);

                        //play music
                        if(mediaPlayer != null) {
                            mediaPlayer.stop();
                            mediaPlayer.release();
                            mediaPlayer = null;
                        }

                        mediaPlayer = MediaPlayer.create(getApplicationContext(), pickedItem.getMusicId());
                        mediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                            @Override
                            public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
                                return false;
                            }
                        });
                        mediaPlayer.setLooping(true);
                        mediaPlayer.start();

                    }
                    else {
                        if(isMusicPlaying == true && mediaPlayer != null) {
                            mediaPlayer.pause();
                            length = mediaPlayer.getCurrentPosition();
                            isMusicPlaying = false;
                        }else if(mediaPlayer != null) {
                            mediaPlayer.seekTo(length);
                            mediaPlayer.start();
                            isMusicPlaying = true;
                        }
                    }
                    Toast.makeText(getApplicationContext(),"picked "+pickedPosition,Toast.LENGTH_SHORT).show();

                }
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) { }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) { }
        });

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(pickedItem == null)
                    pickedItem = myMusicSet.get(0);
                Intent intent = new Intent();
                intent.putExtra("result",pickedItem.getMusicId());
                //Toast.makeText(SelectMusicActivity.this, "selected musicId: "+pickedItem.getImageId(), Toast.LENGTH_SHORT).show();
                setResult(Activity.RESULT_OK, intent);
                finish();
            }
        });
    }


    public void pickItem(int ind) {
        if(pickedItem != null) {
            myMusicSet.get(pickedPosition).setIsPicked(false);
            mAdapter.notifyItemChanged(pickedPosition);
        }
        pickedPosition = ind;
        myMusicSet.get(pickedPosition).setIsPicked(true);
        pickedItem = myMusicSet.get(pickedPosition);
        mAdapter.notifyItemChanged(pickedPosition);
    }

    @Override
    public void onPause() {
        super.onPause();
        if(mediaPlayer!=null) {
            if(mediaPlayer.isPlaying()) {
                mediaPlayer.stop();
                mediaPlayer.release();
            }
            mediaPlayer = null;
        }
    }
}
