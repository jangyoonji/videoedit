package com.yoonji.snow.videoedit;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.yoonji.snow.videoedit.itemview.DatePickerUtil;
import com.yoonji.snow.videoedit.itemview.MyAdapter;
import com.yoonji.snow.videoedit.itemview.TimelineItem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

@RequiresApi(api = Build.VERSION_CODES.M)
public class MainActivity extends AppCompatActivity {
    final String TAG = "MainActivity";
    private final boolean VERBOSE = false;
    static final int PICK_MUSIC_REQUEST = 1;
    final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 99;
    final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 100;
    final int REQ_CODE_SELECT_IMAGE = 100;
    final int REQ_CODE_SELECT_VIDEO = 101;

    private RecyclerView mRecyclerView;
    private DatePicker mStartDatePicker;
    private DatePicker mEndDatePicker;

    private Button addImageButton;
    private Button addVideoButton;
    private ImageButton loadButton;
    private ImageButton createButton;
    private ImageButton musicButton;

    private ProgressBar progressBar;

    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private ArrayList<TimelineItem> myDataSet;
    static public ArrayList<TimelineItem> pickedDataset;
    private int startYear, startMonth, startDate;
    private int endYear, endMonth, endDate;
    private int musicID = R.raw.humidity;
    private int pickedItemNum;


    private GestureDetector gestureDetector;

    private int ThumbnailWidth;
    int[] loadedIndex = new int[100];


    private Handler mhandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG,"111");
        super.onCreate(savedInstanceState);
        Log.d(TAG,"111-222");
        setContentView(R.layout.activity_main);

        Log.d(TAG,"111- 333");

        moveTo(StartPage.class);
        Arrays.fill(loadedIndex,-1);

        DisplayMetrics dm = getApplicationContext().getResources().getDisplayMetrics();
        ThumbnailWidth = dm.widthPixels/4;


        if(checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
        }
        if(checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
        }

        myDataSet = new ArrayList<TimelineItem>();
        pickedDataset = new ArrayList<TimelineItem>();

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mStartDatePicker = (DatePicker) findViewById(R.id.startDatePicker);
        mEndDatePicker = (DatePicker) findViewById(R.id.endDatePicker);

        DatePickerUtil.setFont(this,mStartDatePicker);
        DatePickerUtil.setFont(this,mEndDatePicker);

        addImageButton = (Button) findViewById(R.id.addImageButton);
        addVideoButton = (Button) findViewById(R.id.addVideoButton);
        loadButton = (ImageButton) findViewById(R.id.loadButton);
        createButton = (ImageButton) findViewById(R.id.createButton);
        musicButton = (ImageButton) findViewById(R.id.musicButton);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new GridLayoutManager(this, 4);
        mLayoutManager.setItemPrefetchEnabled(true);

        mRecyclerView.setLayoutManager(mLayoutManager);

        mRecyclerView.setNestedScrollingEnabled(false);

        mAdapter = new MyAdapter(myDataSet);
        mRecyclerView.setAdapter(mAdapter);

        pickedItemNum=0;
        startYear = 2000;
        startMonth = 1;
        startDate = 1;
        endYear = 2000;
        endMonth = 2;
        endDate = 2;

        mRecyclerView.addOnItemTouchListener(onItemTouchListener);


        addImageButton.setOnClickListener(onClickListener);
        addVideoButton.setOnClickListener(onClickListener);
        loadButton.setOnClickListener(onClickListener);
        createButton.setOnClickListener(onClickListener);
        musicButton.setOnClickListener(onClickListener);

        gestureDetector = new GestureDetector( getApplicationContext(),new GestureDetector.SimpleOnGestureListener()
        {

            @Override
            public boolean onSingleTapUp(MotionEvent e)
            {
                return true;
            }

            @Override
            public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
                return super.onScroll(e1, e2, distanceX, distanceY);
            }
        });
        setButtonState();
    }

    private void moveTo(Class<?> cls) {
        Intent intent = new Intent(this, cls);
        startActivity(intent);
    }
    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int id = view.getId();
            Intent intent;
            switch(id) {
                case R.id.addImageButton:
                    intent = new Intent(Intent.ACTION_PICK);
                    intent.setType(android.provider.MediaStore.Images.Media.CONTENT_TYPE);
                    intent.setData(android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, REQ_CODE_SELECT_IMAGE);
                    break;

                case R.id.addVideoButton:
                    intent = new Intent(Intent.ACTION_PICK);
                    intent.setType("video/*");
                    startActivityForResult(
                            Intent.createChooser(intent,"Choose a video"),
                            REQ_CODE_SELECT_VIDEO);
                    break;

                case R.id.loadButton:
                    new LoadAsyncTask().execute();
                    loadButton.setEnabled(false);
                    loadButton.setImageDrawable(getDrawable(R.drawable.ok_enabled_button));


                    break;

                case R.id.createButton:
                    pickedDataset.clear();
                    TimelineItem timelineItem;
                    for(int i=0; i<myDataSet.size(); i++) {
                        timelineItem = myDataSet.get(i);
                        if(timelineItem.getIsPicked() && !timelineItem.getIsError()) {
                            if(timelineItem.getIsHighlight() == true) {
                                timelineItem.setDuration((int)(1.5*timelineItem.getDuration()));
                            }
                            pickedDataset.add(timelineItem);
                        }
                    }

                    intent = new Intent(getApplicationContext(), CreateVideoActivity.class);
                    //intent.putParcelableArrayListExtra("timelineItems",pickedDataset);
                    intent.putExtra("musicID",musicID);
                    startActivity(intent);
                    break;

                case R.id.musicButton:
                    intent = new Intent(getApplicationContext(), SelectMusicActivity.class);
                    startActivityForResult(intent, PICK_MUSIC_REQUEST);
                    break;

            }
        }
    };



    RecyclerView.OnItemTouchListener onItemTouchListener = new RecyclerView.OnItemTouchListener() {
        private long startClickTime;
        private int pos;
        private boolean isClick;

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            //Log.d(TAG,"onInterceptTouchEvent");
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            //&&gestureDetector.onTouchEvent(e)
            if(child != null && e.getActionMasked() == MotionEvent.ACTION_DOWN) {
                startClickTime = System.currentTimeMillis();
                pos = rv.getChildAdapterPosition(child);
                //isClick = true;
            }
            else if(rv.getScrollState() != RecyclerView.SCROLL_STATE_DRAGGING &&
            child != null && e.getActionMasked() == MotionEvent.ACTION_UP)
            {
                int position = rv.getChildAdapterPosition(child);

                if (System.currentTimeMillis() - startClickTime < ViewConfiguration.getTapTimeout() &&
                        pos == position) {

                    if(position >= 0) {
                        if(myDataSet.get(position).getIsHighlight() == true) {
                            myDataSet.get(position).setIsPicked(false);
                            myDataSet.get(position).setIsHighlight(false);
                        }
                        else if(myDataSet.get(position).getIsPicked() == true)
                            myDataSet.get(position).setIsHighlight(true);

                        else if(myDataSet.get(position).getIsPicked() == false)
                            myDataSet.get(position).setIsPicked(true);

                        mAdapter.notifyItemChanged(position);
                        if (myDataSet.get(position).getIsPicked() == false)
                            pickedItemNum--;
                        else
                            pickedItemNum++;
                        setButtonState();
                    }
                    else
                        Log.d(TAG,"position: "+position);

                    //isClick =false;
                    return false;
                }

            }


            return false;

        }


        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {  }
    };

    public void setButtonState() {
        if(myDataSet.size() == 0) {
            createButton.setEnabled(false);
            createButton.setImageDrawable(getDrawable(R.drawable.create_enabled_button));
        }
        else if (pickedItemNum <= 0) {
            createButton.setEnabled(false);
            createButton.setImageDrawable(getDrawable(R.drawable.create_enabled_button));

        }
        else {
            createButton.setEnabled(true);
            createButton.setImageDrawable(getDrawable(R.drawable.create_button));

        }


    }


    public class LoadAsyncTask extends AsyncTask<Void, Integer, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            startYear = mStartDatePicker.getYear();
            startMonth = mStartDatePicker.getMonth();
            startDate = mStartDatePicker.getDayOfMonth();

            endYear = mEndDatePicker.getYear();
            endMonth = mEndDatePicker.getMonth();
            endDate = mEndDatePicker.getDayOfMonth();

            myDataSet.clear();
            mAdapter.notifyDataSetChanged();
            progressBar.setVisibility(View.GONE);

        }

        @Override
        protected Void doInBackground(Void... voids) {
            String ExternalStorageDirectoryPath = Environment
                    .getExternalStorageDirectory()
                    .getAbsolutePath();
            queryImageOrVideoFiles();

            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);

            progressBar.setProgress(values[0]);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mAdapter.notifyDataSetChanged();
            Toast.makeText(getApplicationContext(),"Loaded "+myDataSet.size()+" files ",Toast.LENGTH_SHORT).show();
            progressBar.setVisibility(View.GONE);
            progressBar.setProgress(0);
            pickedItemNum = myDataSet.size();
            setButtonState();


            loadButton.setEnabled(true);
            loadButton.setImageDrawable(getDrawable(R.drawable.ok_button));

        }

        public void queryImageOrVideoFiles() {
            Calendar calendar = Calendar.getInstance();
            calendar.getTime();

            // External 스토리지의 URI 획득
            final Uri uri = MediaStore.Files.getContentUri("external");
            //ID, 파일명, mimeType, 파일크기 을 가져오도록 설정
            final String[] projection =
                    new String[] {MediaStore.Files.FileColumns._ID,
                            MediaStore.Files.FileColumns.DISPLAY_NAME,
                            MediaStore.Files.FileColumns.MEDIA_TYPE,
                            MediaStore.Files.FileColumns.MIME_TYPE,
                            MediaStore.Files.FileColumns.SIZE,
                            MediaStore.Images.Media.HEIGHT,
                            MediaStore.Images.Media.WIDTH,
                            MediaStore.Images.Media.DATE_TAKEN,
                            MediaStore.Video.Media.DURATION,
                            MediaStore.Images.Media.DATA,
                            MediaStore.Images.Media.ORIENTATION};

            String selection = null;
            selection = MediaStore.Images.Media.DATE_TAKEN + ">="
                    + getMillis(startYear, startMonth, startDate) + " AND "
                    + MediaStore.Images.Media.DATE_TAKEN + "<="
                    + getMillis(endYear, endMonth, endDate) + " AND "
                    + MediaStore.Files.FileColumns.MEDIA_TYPE
                    + " != "
                    + MediaStore.Files.FileColumns.MEDIA_TYPE_AUDIO + " AND "
                    + MediaStore.Files.FileColumns.MEDIA_TYPE
                    + " != "
                    + MediaStore.Files.FileColumns.MEDIA_TYPE_NONE + " AND "
                    + MediaStore.Files.FileColumns.MEDIA_TYPE
                    + " != "
                    + MediaStore.Files.FileColumns.MEDIA_TYPE_PLAYLIST;



            // 쿼리 수행 후, 컬럼명, 값 출력

            Cursor cursor2 = getContentResolver().query(uri,
                    projection,
                    selection,
                    null,
                    //MediaStore.Files.FileColumns.DATE_ADDED + " ASC"); //DESC: 내림차순
                    MediaStore.Images.Media.DATE_TAKEN + " ASC");

            addItem(cursor2);

        }

        public void addItem(Cursor cursor) {
            int size=0;
            while (cursor != null && cursor.moveToNext()) {
                size++;
            }
            cursor.moveToFirst();
            if (cursor != null && cursor.moveToFirst()) {

                do {
                    int columnCount = cursor.getColumnCount();

                    if(VERBOSE)
                        for (int i = 0 ; i < columnCount ; i++ ) {
                             Log.d(TAG,cursor.getColumnName(i) + " : " + cursor.getString(i));
                        }


                    TimelineItem timelineItem = new TimelineItem();
                    timelineItem.init(cursor);

                    myDataSet.add(myDataSet.size(), timelineItem);

                    //Illegal to call this method when user is scrolling view
                    //mAdapter.notifyItemInserted(myDataSet.size());

                    /*
                    final Runnable r = new Runnable() {
                        @Override
                        public void run() {
                            mAdapter.notifyItemInserted(myDataSet.size()-1);
                        }
                    };
                    mhandler.post(r);
                    */

                    //Only the original thread that created a view hierachy can touch its views
                    //mAdapter.notifyDataSetChanged();
                    if(VERBOSE) Log.d(TAG,"--------------------------");
                    if(myDataSet.size() % 3 == 0)
                        publishProgress((int)((float)myDataSet.size()/(float)size*100));
                    //Log.d(TAG,"progress "+(int)((float)myDataSet.size()/(float)size*100) +"%");
                } while (cursor.moveToNext());
            }
        }
    }

    //not use
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(requestCode) {
            case REQ_CODE_SELECT_IMAGE:
                if (resultCode == Activity.RESULT_OK) {
                    try {
                        Uri selectedImage = data.getData();
                        //TimelineItem timelineItem = getTimelineItemFromUri(selectedImage, true);
                        //addItem(timelineItem);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    Toast.makeText(getBaseContext(), "resultCode : " + resultCode, Toast.LENGTH_SHORT).show();
                }
            break;

            case REQ_CODE_SELECT_VIDEO:
                if (resultCode == Activity.RESULT_OK) {
                    try {
                        Uri selectedVideo = data.getData();
                        //addItem(timelineItem);
                    }  catch (Exception e) {
                        e.printStackTrace();
                    }
                    Toast.makeText(getBaseContext(), "resultCode : " + resultCode, Toast.LENGTH_SHORT).show();
                }
                break;

            case PICK_MUSIC_REQUEST :
                if(resultCode == Activity.RESULT_OK) {
                    musicID = data.getIntExtra("result",0);
                    Toast.makeText(getApplicationContext(),"musicID: "+musicID,Toast.LENGTH_SHORT).show();
                }
                else
                    Toast.makeText(getApplicationContext(),"not ok: ",Toast.LENGTH_SHORT).show();

                break;

        }
    }

    private long getMillis(int year, int month, int date) {
        Calendar searchCalendar = Calendar.getInstance();

        Log.d(TAG, " current date: " + year + " / " + month + " / " + date);
        searchCalendar.set(Calendar.YEAR, year);
        searchCalendar.set(Calendar.MONTH, month);
        searchCalendar.set(Calendar.DAY_OF_MONTH, date);
        Log.d(TAG,
                "searchCalendar time: " + searchCalendar.getTimeInMillis());

        return searchCalendar.getTimeInMillis();
    }



    class LoadThread1 extends Thread {
        /*
        int i=0;
        int ind=0;
        @Override
        public void run() {

            Looper.prepare();
            handler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    super.handleMessage(msg);
                    if(msg.arg1>=0 && msg.arg1 < myDataSet.size()) {
                        //myDataSet.get(msg.arg1).setBitmapThumbnail(getApplicationContext(), MediaStore.Video.Thumbnails.MICRO_KIND, ThumbnailWidth/4);
                        //mAdapter.notifyItemChanged(msg.arg1);
                        //notifyDataSetChanged();
                        ind = msg.arg1;
                        Runnable r = new Runnable() {
                            @Override
                            public void run() {
                                //mAdapter.notifyDataSetChanged();
                                //mAdapter.notifyItemChanged(ind);
                            }
                        };
                        mhandler.post(r);
                    }
                }
            };
            Looper.loop();
        }
        */
    }

    class LoadThread extends Thread {
        /*
        int i=0;
        @Override
        public void run() {
            while(true) {

                Runnable r = new Runnable() {
                    @Override
                    public void run() {
                        mAdapter.notifyDataSetChanged();
                    }
                };
                if(i<myDataSet.size() && !isInterrupted()) {
                    if (!myDataSet.get(i).setBitmapThumbnail(getApplicationContext(), MediaStore.Video.Thumbnails.MICRO_KIND, ThumbnailWidth / 4) ||
                            myDataSet.get(i).getIsError()) {
                        //myDataSet.remove(i);
                        i++;
                        Log.d(TAG, "remove invalid item");
                    } else
                        i++;
                    mhandler.post(r);
                }
                else
                    break;
            }
        }
        */
    }



    public static ArrayList<TimelineItem> getPickedDataset() {
        return pickedDataset;
    }
    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

}
