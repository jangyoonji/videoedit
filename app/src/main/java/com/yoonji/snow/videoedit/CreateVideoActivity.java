package com.yoonji.snow.videoedit;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.yoonji.snow.videoedit.itemview.TimelineAdapter;
import com.yoonji.snow.videoedit.itemview.TimelineItem;
import com.yoonji.snow.videoedit.mediacodec.EncodeSaveThread;
import com.yoonji.snow.videoedit.mediacodec.VideoDecoderThread;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by snow on 2017-08-01.
 */
@RequiresApi(api = Build.VERSION_CODES.M)
public class CreateVideoActivity  extends Activity{
    private final String TAG = "VideoEditActivity";
    private final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 1;
    private final int REQ_CODE_EDIT_DURATION = 2;
    private final int ENCODE_IMAGE = 2;

    private VideoDecoderThread mVideoDecoder;
    private EncodeSaveThread mEncodeSaver;

    private SurfaceView surfaceView;
    //private MseVideoView videoView;
    private RecyclerView recyclerView;
    private ImageButton editButton;
    private ImageButton saveButton;
    private ImageButton start_pauseButton;
    private ImageButton displayButton;

    private ProgressBar progressBar;

    private boolean isStreaming;

    private ArrayList<TimelineItem> myDataSet;
    private TimelineItem pickedItem;

    private String[] Pathes;
    private String output_path;

    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private int pickedPosition;
    private int musicID;

    boolean decodingFinished;

    private int ThumbnailWidth;
    private DisplayMetrics displayMetrics;

    private MediaPlayer mediaPlayer;
    private int length;

    private String time;
    private File outputFile;
    private File temp;
    private String temp_path;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_video);

        surfaceView = (SurfaceView) findViewById(R.id.videoView);
        displayMetrics = getApplicationContext().getResources().getDisplayMetrics();
        ThumbnailWidth = displayMetrics.widthPixels;
        int height = (int)((float)displayMetrics.widthPixels/(float)1024*(float)768);
        RelativeLayout.LayoutParams param = new RelativeLayout.LayoutParams(displayMetrics.widthPixels,height);
        surfaceView.setLayoutParams(param);


        decodingFinished = true;

        editButton = (ImageButton) findViewById(R.id.editButton);
        saveButton = (ImageButton)  findViewById(R.id.saveButton);
        start_pauseButton = (ImageButton) findViewById(R.id.start_pauseButton);
        displayButton = (ImageButton) findViewById(R.id.displayButton);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        recyclerView = (RecyclerView) findViewById(R.id.edit_timelineView);
        recyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
        recyclerView.setLayoutManager(mLayoutManager);

        myDataSet = new ArrayList<TimelineItem>();
        mAdapter = new TimelineAdapter(myDataSet);
        recyclerView.setAdapter(mAdapter);

        pickedPosition = 0;

        isStreaming = false;

        mVideoDecoder = new VideoDecoderThread();
        mEncodeSaver = new EncodeSaveThread();

        if(checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
        }

        Bundle bundle = getIntent().getExtras();
        musicID = bundle.getInt("musicID");

//        ArrayList<TimelineItem> tempDataSet = bundle.getParcelableArrayList("timelineItems");
        //tempDataSet.get(pickedPosition).setIsSelected(true);

        myDataSet.addAll(MainActivity.getPickedDataset());

        myDataSet.get(pickedPosition).setIsSelected(true);
        pickedItem = myDataSet.get(pickedPosition);
        mAdapter.notifyDataSetChanged();
        
        Toast.makeText(getApplicationContext(),"Adapter Item size: "+mAdapter.getItemCount(),Toast.LENGTH_SHORT).show();


        editButton.setOnClickListener(onClickListener);
        saveButton.setOnClickListener(onClickListener);
        start_pauseButton.setOnClickListener(onClickListener);
        displayButton.setOnClickListener(onClickListener);

        final GestureDetector gestureDetector = new GestureDetector(this,new GestureDetector.SimpleOnGestureListener()
        {
            @Override
            public boolean onSingleTapUp(MotionEvent e)
            {
                return true;
            }
        });

        //touch item --> draw on SurfaceView if it is an image
        recyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                //Log.d(TAG,"onInterceptTouchEvent");
                View child = rv.findChildViewUnder(e.getX(), e.getY());

                if(child!=null&&gestureDetector.onTouchEvent(e))
                {
                    int position = rv.getChildAdapterPosition(child);
                    TimelineItem timelineItem = myDataSet.get(position);
                    Log.d(TAG,"Selected position:  "+position + ", is Image: "+ Boolean.toString(timelineItem.getIsImage()));

                    pickItem(position);
                    Toast.makeText(getApplicationContext(), Boolean.toString(timelineItem.getIsSimilarImage()),Toast.LENGTH_SHORT).show();

                }
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {  }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {  }
        });


    }

    Handler handler_decode = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            int a = msg.arg1;

            if(a=='s') {
                //play music
                if(mediaPlayer != null) {
                    mediaPlayer.stop();
                    mediaPlayer.release();
                    mediaPlayer = null;
                }

                mediaPlayer = MediaPlayer.create(getApplicationContext(), musicID);
                mediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                    @Override
                    public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
                        return false;
                    }
                });
                mediaPlayer.setLooping(true);
                int currentPosition = 0;
                for(int i=0; i<msg.arg2; i++) {
                    currentPosition += myDataSet.get(i).getDuration();
                }
                mediaPlayer.seekTo(currentPosition % mediaPlayer.getDuration());
                mediaPlayer.start();
            }
            if(a == 'r') {
                int b = msg.arg2;
                if(b >= 0 && b < myDataSet.size()) {
                    Log.d(TAG,"a: "+b);
                    pickItem(b);
                    recyclerView.scrollToPosition(b);
                }
            }
            if(a == 'q') {
                Log.d(TAG,"decoding finished, stop music");
                mediaPlayer.stop();
                isStreaming = false;
                decodingFinished = true;

                //start_pauseButton.setText("START");
                start_pauseButton.setImageDrawable(getDrawable(R.drawable.start_button));
                displayButton.setEnabled(true);
                displayButton.setImageDrawable(getDrawable(R.drawable.display_button));
            }

        }
    };
    Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            progressBar.setProgress(0);

            Toast.makeText(getApplicationContext(),"Copy File to Gallery",Toast.LENGTH_SHORT).show();

            //start_pauseButton.setEnabled(true);
            File file = new File(Environment.getExternalStorageDirectory(),"VideoEdit");
            if(!file.exists())
                file.mkdir();
            if(time != null)
                outputFile = new File(file, time+".mp4");
            else
                Log.d(TAG,"time is null");
            try {
                copyFile(temp.toString(), outputFile.toString());
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(getApplicationContext(),"Save Fail!",Toast.LENGTH_SHORT).show();
                return;
            }
            //output_path = outputFile.toString();
            saveButton.setEnabled(true);
            editButton.setEnabled(true);
            saveButton.setImageDrawable(getApplicationContext().getDrawable(R.drawable.save_button));
            editButton.setImageDrawable(getApplicationContext().getDrawable(R.drawable.edit_button));

            Toast.makeText(getApplicationContext(),"Save Done!",Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            Uri contentUri = Uri.fromFile(outputFile);
            intent.setData(contentUri);
            getApplicationContext().sendBroadcast(intent);
        }
    };

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int id = view.getId();
            Intent intent;
            switch (id) {
                case R.id.editButton:
                    intent = new Intent(getApplicationContext(), ChangeDurationActivity.class);
                    intent.putExtra("pickedItem",pickedItem);
                    startActivityForResult(intent, REQ_CODE_EDIT_DURATION);

                    break;
                case R.id.start_pauseButton:

                    isStreaming =! isStreaming;
                    if(isStreaming == true) {
                        if(decodingFinished) {
                            decodingFinished = false;
                            //start_pauseButton.setText("STOP");
                            start_pauseButton.setImageDrawable(getDrawable(R.drawable.stop_button));
                            displayButton.setEnabled(false);
                            displayButton.setImageDrawable(getDrawable(R.drawable.display_disabled_button));

                            mVideoDecoder = new VideoDecoderThread();
                            if (mVideoDecoder.init(surfaceView, myDataSet, pickedPosition, handler_decode)) {
                                mVideoDecoder.run();
                                Log.d(TAG, "video Decoder Start");
                            } else
                                Log.d(TAG, "init failed");
                        }
                        else {
                            isStreaming = false;
                            start_pauseButton.setImageDrawable(getDrawable(R.drawable.start_button));
                            displayButton.setEnabled(true);
                            displayButton.setImageDrawable(getDrawable(R.drawable.display_button));

                            Log.d(TAG,"wait for decoding finished");
                        }
                    }
                    else {
                        //start_pauseButton.setText("START");
                        start_pauseButton.setImageDrawable(getDrawable(R.drawable.start_button));
                        displayButton.setEnabled(true);
                        displayButton.setImageDrawable(getDrawable(R.drawable.display_button));

                        if (mVideoDecoder != null) {
                            mVideoDecoder.close();
                            Log.d("TAG", "VideoDecoder destroyed1");

                            if(mediaPlayer != null && mediaPlayer.isPlaying())
                                mediaPlayer.stop();
                        }
                    }


                    break;

                case R.id.saveButton:
                    saveButton.setEnabled(false);
                    editButton.setEnabled(false);
                    saveButton.setImageDrawable(getApplicationContext().getDrawable(R.drawable.save_enabled_button));
                    editButton.setImageDrawable(getApplicationContext().getDrawable(R.drawable.edit_enabled_button));

                    if(mEncodeSaver != null) {
                        mEncodeSaver = new EncodeSaveThread();
                    }

                    Dialog dialog = onCreateDialogSingleChoice(false);
                    dialog.show();

                    break;
                case R.id.displayButton:
                    Dialog dialog_preview = onCreateDialogSingleChoice(true);
                    dialog_preview.show();
                    break;
            }
        }
    };

    private void copyFile(String source, String target) {
        File sourceFile = new File(source);
        FileInputStream inputStream = null;
        FileOutputStream outputStream = null;

        FileChannel fcin = null;
        FileChannel fcout = null;

        try {
            inputStream = new FileInputStream(sourceFile);
            outputStream = new FileOutputStream(target);

            fcin = inputStream.getChannel();
            fcout= outputStream.getChannel();

            long size = fcin.size();
            fcin.transferTo(0, size, fcout);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fcout.close();
                fcin.close();
                inputStream.close();
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private int pos=0;
    public Dialog onCreateDialogSingleChoice(final boolean isPreview) {
        //Initialize the Alert Dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        //Source of the data in the DIalog
        final int[] width = {1024, 1024, 1024};
        final int[] height = {768, 1280, 1024};
        CharSequence[] array = {"1024 * 768","1024 * 1280", "1024 * 1024"};
        // Set the dialog title
        builder.setTitle("Select Output Size")
                // Specify the list array, the items to be selected by default (null for none),
                // and the listener through which to receive callbacks when items are selected
                .setSingleChoiceItems(array, pos, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        pos=which;
                    }
                })
                    // Set the action buttons
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                    // User clicked OK, so save the result somewhere
                    // or return them to the component that opened the dialog
                        Toast.makeText(getApplicationContext(),width[pos]+" * "+height[pos],Toast.LENGTH_SHORT).show();

                        if(!isPreview) {
                            SimpleDateFormat sdfNow = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
                            time = sdfNow.format(new Date(System.currentTimeMillis()));

                            temp = new File(getFilesDir(), time + ".mp4");
                            temp_path = temp.toString();

                            mEncodeSaver.init(temp_path, myDataSet, musicID, width[pos], height[pos], 6000000, 0.5f, handler, progressBar, getApplicationContext());
                            mEncodeSaver.start();
                        }
                        else {
                            int size = (int)((float)displayMetrics.widthPixels/(float)width[pos]*(float)height[pos]);
                            RelativeLayout.LayoutParams param = new RelativeLayout.LayoutParams(displayMetrics.widthPixels,size);
                            surfaceView.setLayoutParams(param);
                            mAdapter.notifyDataSetChanged();
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        saveButton.setEnabled(true);
                        editButton.setEnabled(true);
                        saveButton.setImageDrawable(getApplicationContext().getDrawable(R.drawable.save_button));
                        editButton.setImageDrawable(getApplicationContext().getDrawable(R.drawable.edit_button));

                        displayButton.setEnabled(true);
                        displayButton.setImageDrawable(getDrawable(R.drawable.display_button));
                    }
                });
        builder.setCancelable(true);
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                saveButton.setEnabled(true);
                editButton.setEnabled(true);
                saveButton.setImageDrawable(getApplicationContext().getDrawable(R.drawable.save_button));
                editButton.setImageDrawable(getApplicationContext().getDrawable(R.drawable.edit_button));
            }
        });
        return builder.create();
    }
    @Override
    public void onPause() {
        super.onPause();
        if(mediaPlayer != null && mediaPlayer.isPlaying())
            mediaPlayer.pause();
    }

    public void pickItem(int ind) {
        myDataSet.get(pickedPosition).setIsSelected(false);
        mAdapter.notifyItemChanged(pickedPosition);
        pickedPosition = ind;
        myDataSet.get(pickedPosition).setIsSelected(true);
        pickedItem = myDataSet.get(pickedPosition);
        mAdapter.notifyItemChanged(pickedPosition);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(requestCode) {
            case REQ_CODE_EDIT_DURATION :
                if(resultCode == Activity.RESULT_OK) {
                    long duration =  data.getLongExtra("result",0);
                    pickedItem.setDuration(duration);
                    Toast.makeText(getApplicationContext(),"duration: "+duration,Toast.LENGTH_SHORT).show();
                }
                else
                    Toast.makeText(getApplicationContext(),"not ok: ",Toast.LENGTH_SHORT).show();

                break;

        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mVideoDecoder != null) {
            mVideoDecoder.close();
            Log.d("TAG", "VideoDecoder destroyed2");
        }
        if(mediaPlayer != null) {
            if(mediaPlayer.isPlaying())
                mediaPlayer.stop();
        }
        if(temp != null) {
            temp.delete();
            Log.d(TAG,"delete temp file in internal storage");
        }

    }
}
