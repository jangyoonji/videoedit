package com.yoonji.snow.videoedit.itemview;

import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by snow on 2017-08-09.
 */

public class MusicItem implements Parcelable{

    public double duration;
    public String mTitle;
    public int musicId;
    public Drawable mDrawable;
    public boolean isPicked;

    public MusicItem() {
        duration = 0;
        mTitle = null;
        musicId = 0;
        mDrawable = null;
        isPicked = false;
    }

    public MusicItem(double duration, String title, int id, Drawable drawable) {
        this.duration = duration;
        mTitle = title;
        musicId = id;
        mDrawable = drawable;
        isPicked = false;
    }

    public MusicItem(Parcel in) {
        readFromParcel(in);
    }

    public int getMusicId() { return musicId; }
    public String getTitle() { return mTitle; }
    public Drawable getDrawable() { return mDrawable; }
    public boolean getIsPicked() { return isPicked; }
    public void setIsPicked(boolean tf) { isPicked = tf; }


    @Override
    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {

        dest.writeDouble(duration);
        dest.writeString(mTitle);

    }

    public void readFromParcel(Parcel in) {
        duration = in.readDouble();
        mTitle = in.readString();
    }

    public static final Parcelable.Creator CREATOR =
            new Parcelable.Creator() {
                public MusicItem createFromParcel(Parcel in) {
                    return new MusicItem(in);
                }

                public MusicItem[] newArray(int size) {
                    return new MusicItem[size];
                }
            };
}
