package com.yoonji.snow.videoedit.mediacodec;

/**
 * Created by snow on 2017-08-01.
 */

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaCodec;
import android.media.MediaExtractor;
import android.media.MediaFormat;
import android.opengl.GLES20;
import android.opengl.GLUtils;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.Surface;

import java.nio.ByteBuffer;

import android.view.SurfaceView;

import com.yoonji.snow.videoedit.itemview.TimelineItem;
import com.yoonji.snow.videoedit.glutils.GLUtil;
import com.yoonji.snow.videoedit.glutils.InputSurface;
import com.yoonji.snow.videoedit.glutils.OutputSurface;

import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.ArrayList;

import static junit.framework.Assert.assertTrue;

@RequiresApi(api = Build.VERSION_CODES.M)
public class VideoDecoderThread{
    private final boolean VERBOSE = false;
    private static final String VIDEO = "video/";
    private static final String TAG = "VideoDecoder";
    private MediaExtractor mExtractor;
    private MediaCodec mDecoder;
    private boolean eosReceived;
    private Surface mSurface;

    private ArrayList<TimelineItem> timelineItems;
    private TimelineItem currentTimelineItem;
    private int fileInd;

    private InputSurface inputSurface;
    private OutputSurface outputSurface;

    public static final String NO_FILTER_VERTEX_SHADER = "" +
            "attribute vec4 position;\n" +
            "attribute vec4 inputTextureCoordinate;\n" +
            " \n" +
            "varying vec2 textureCoordinate;\n" +
            " \n" +
            "void main()\n" +
            "{\n" +
            "    gl_Position = position;\n" +
            "    textureCoordinate = inputTextureCoordinate.xy;\n" +
            "}";
    public static final String NO_FILTER_FRAGMENT_SHADER = "" +
            "varying highp vec2 textureCoordinate;\n" +
            " \n" +
            "uniform sampler2D inputImageTexture;\n" +
            " \n" +
            "void main()\n" +
            "{\n" +
            "     gl_FragColor = texture2D(inputImageTexture, textureCoordinate);\n" +
            "}";

    //for video
    // Replaces TextureRender.FRAGMENT_SHADER during edit; swaps green and blue channels.
    private static final String VIDEO_FRAGMENT_SHADER =
            "#extension GL_OES_EGL_image_external : require\n" +
                    "precision mediump float;\n" +      // highp here doesn't seem to matter
                    "varying vec2 vTextureCoord;\n" +
                    "uniform samplerExternalOES sTexture;\n" +
                    "void main() {\n" +
                    "  gl_FragColor = texture2D(sTexture, vTextureCoord);\n" +
                    "}\n";

    private int mWidth;
    private int mHeight;

    private Handler mHandler;
    public boolean init(SurfaceView surfaceView, ArrayList<TimelineItem> timelineitems, int fileIndex, Handler handler) {
        eosReceived = false;
        mSurface = surfaceView.getHolder().getSurface();
        timelineItems = timelineitems;
        fileInd=fileIndex;

        inputSurface = null;
        outputSurface = null;

        mWidth = surfaceView.getWidth();
        mHeight = surfaceView.getHeight();

        mHandler = handler;
        return true;
    }
    static Thread th;
    private static class VideoDecodeWrapper implements Runnable {
        private Throwable mThrowable;
        private VideoDecoderThread mTest;

        private VideoDecodeWrapper(VideoDecoderThread test) {
            mTest = test;
        }

        @Override
        public void run() {
            try {
                mTest.decode();
            } catch (Throwable th) {
                mThrowable = th;
            }
        }

        /** Entry point. */
        public static void runTest(VideoDecoderThread obj) throws Throwable {
            VideoDecoderThread.VideoDecodeWrapper wrapper = new VideoDecoderThread.VideoDecodeWrapper(obj);
            th = new Thread(wrapper, "codec test");
            th.start();
            if (wrapper.mThrowable != null) {
                throw wrapper.mThrowable;
            }
        }
    }

    public void decode() {
        boolean isFirstTime = true;
        while (fileInd < timelineItems.size() && !Thread.currentThread().isInterrupted()) {

            try {
                if (Thread.currentThread().isInterrupted())
                    Log.d(TAG, "is interrupted");
                while (InitDecodingIndex(fileInd++, isFirstTime) == false) ;
                isFirstTime = false;
                //fileInd++;
                frameIndex = 0;
                generateIndex = 0;
                isFade = true;


                MediaCodec.BufferInfo info = new MediaCodec.BufferInfo();
                boolean first = false;
                long startWhen = 0;

                if (!currentTimelineItem.getIsImage() && !Thread.currentThread().isInterrupted()) {

                    while (!eosReceived && !Thread.currentThread().isInterrupted()) {
                        int inputIndex = mDecoder.dequeueInputBuffer(10000);
                        if (inputIndex >= 0) {
                            ByteBuffer inputBuffer = mDecoder.getInputBuffer(inputIndex);
                            int sampleSize = mExtractor.readSampleData(inputBuffer, 0);
                            if (mExtractor.advance() && sampleSize > 0) {
                                mDecoder.queueInputBuffer(inputIndex, 0, sampleSize, mExtractor.getSampleTime(), 0);
                            } else {
                                Log.d(TAG, "InputBuffer BUFFER_FLAG_END_OF_STREAM");
                                mDecoder.queueInputBuffer(inputIndex, 0, 0, 0, MediaCodec.BUFFER_FLAG_END_OF_STREAM);
                                eosReceived = true;
                            }
                        }

                        int outIndex = mDecoder.dequeueOutputBuffer(info, 10000);
                        if (outIndex >= 0) {
                            switch (outIndex) {
                                case MediaCodec.INFO_OUTPUT_FORMAT_CHANGED:
                                    Log.d(TAG, "INFO_OUTPUT_FORMAT_CHANGED format : " + mDecoder.getOutputFormat());
                                    break;
                                case MediaCodec.INFO_TRY_AGAIN_LATER:
                                    Log.d(TAG, "INFO_TRY_AGAIN_LATER");

                                    break;
                                case MediaCodec.BUFFER_FLAG_END_OF_STREAM:
                                    //eosReceived=true;
                                    Log.d(TAG, "End of Stream");
                                default:
                                    if (!first) {
                                        startWhen = System.currentTimeMillis();
                                        first = true;
                                    }

                                    try {
                                        long sleepTime = (info.presentationTimeUs / 1000) - (System.currentTimeMillis() - startWhen);
                                        if (VERBOSE)
                                            Log.d(TAG, "info.presentationTimeUs : " + (info.presentationTimeUs / 1000) + " playTime: " + (System.currentTimeMillis() - startWhen) + " sleepTime : " + sleepTime);
                                        if (sleepTime > 0)
                                            Thread.sleep(sleepTime);
                                    } catch (InterruptedException e) {
                                        // TODO Auto-generated catch block
                                        e.printStackTrace();
                                        fileInd = timelineItems.size();
                                        eosReceived = true;
                                        break;
                                    }
                                    mDecoder.releaseOutputBuffer(outIndex, true /* Surface init */);

                                    //fade in, fade out
                                    if ((frameIndex < step) || (frameIndex >= (num_frames - step)))
                                        changeFragmentShader(false, frameIndex);
                                    //outputSurface.changeFragmentShader(VIDEO_FRAGMENT_SHADER);
                                    frameIndex++;

                                    outputSurface.awaitNewImage();
                                    outputSurface.drawImage();

                                    if (VERBOSE) Log.d(TAG, "swapBuffers");
                                    inputSurface.swapBuffers();

                                    break;
                            }
                        }

                        // All decoded frames have been rendered, we can stop playing now
                        if ((info.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0) {
                            Log.d(TAG, "OutputBuffer BUFFER_FLAG_END_OF_STREAM");
                            eosReceived = true;
                            break;
                        }
                    }

                } else {
                    while (!eosReceived && !Thread.currentThread().isInterrupted()) {
                        if (VERBOSE) Log.d(TAG, "decoding is image file");
                        if (generateIndex == num_frames) {
                            //decoderOutputAvailable = false;
                            Log.d(TAG, "image file EOS");
                            eosReceived = true;
                            //presentationTimeUsLast = currentImagePresentationTimeUs;
                            //if(VERBOSE) Log.d(TAG,"presentationTimeUsLast: "+presentationTimeUsLast);

                        } else {
                            if(currentTimelineItem.getIsHighlight())
                                //drawBitmap(generateIndex);
                                drawBitmap_highlight(generateIndex);
                            else
                                drawBitmap(generateIndex);
                            if (!first) {
                                startWhen = System.currentTimeMillis();
                                first = true;
                            }
                            try {
                                long sleepTime = (computePresentationTime(generateIndex) / 1000) - (System.currentTimeMillis() - startWhen);
                                //Log.d(TAG, "info.presentationTimeUs : " + (info.presentationTimeUs / 1000) + " playTime: " + (System.currentTimeMillis() - startWhen) + " sleepTime : " + sleepTime);
                                if (sleepTime > 0)
                                    Thread.sleep(sleepTime);
                            } catch (InterruptedException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                                fileInd = timelineItems.size();
                                eosReceived = true;
                            }
                            if (VERBOSE) Log.d(TAG, "inputSurface swapBuffers");
                            inputSurface.swapBuffers();
                        }

                        generateIndex++;
                    }
                }

                if (mSurface.isValid() == false) {
                    break;
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.d(TAG,"decoding #"+fileInd+" failed!");
                continue;
            }
        }

        if (mDecoder != null) {
            Log.d(TAG,"decoder release start");
            mDecoder.stop();
            mDecoder.release();
            Log.d(TAG,"decoder release done");

        }
        if (outputSurface != null) {
            Log.d(TAG,"outputSurface release");
            outputSurface.release();
        }
        if (inputSurface != null) {
            Log.d(TAG,"inputSurface release");
            inputSurface.release();
        }
        message = new Message();
        message.arg1='q';
        //mHandler.sendMessage(message);
        mHandler.sendMessageAtFrontOfQueue(message);
        //if(mSurface != null) {
        //    mSurface.release();
        // }
    }

    public void run() {
        try {
            VideoDecodeWrapper.runTest(this);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    //hi
    float ratio_width=1.0f;
    float ratio_height=1.0f;
    float widthBigger = 1.0f;
    float heightBigger = 1.0f;
    int num_frames;
    float fadeRate = 0.5f;
    private int FRAME_RATE;
    private int frameIndex;
    private int generateIndex;
    private float step;

    public static final float CUBE[] = {
            -1.0f, -1.0f,
            1.0f, -1.0f,
            -1.0f, 1.0f,
            1.0f, 1.0f,
    };

    //for drawing image on surface
    private FloatBuffer mGLCubeBuffer;
    private FloatBuffer mTextureTransformMatrix;
    private int mProgramId;

    protected int mGLAttribPosition;
    protected int mGLUniformTexture;
    protected int mGLAttribTextureCoordinate;
    private Bitmap bitmap;
    private int[] imageTextures = null;


    float rate = 0;
    float rate_black=0;
    boolean isFade = true;

    private Message message;

    int originalWidth;
    int originalHeight;
    int rotation = 0;

    public boolean InitDecodingIndex(int fileInd, boolean isfirst) {

        Log.d(TAG,"try to init decoding #"+fileInd);
        if(mDecoder != null) {
            mDecoder.stop();
            mDecoder.release();
            mDecoder = null;

            mExtractor.release();
            mExtractor = null;

            Log.d(TAG, "releasing decoder, extractor #"+fileInd);
        }
        eosReceived=false;
        if(isfirst) {
            Log.d(TAG,"init first time");
            message = new Message();
            message.arg1 = 's';
            message.arg2 = fileInd;
            mHandler.sendMessage(message);

            try {
                inputSurface = new InputSurface(mSurface);
            } catch (Throwable th) {
                th.printStackTrace();
                Log.d(TAG,"catch error while inputSurface()");
            }

            Log.d(TAG,"new inputsurface");
            inputSurface.makeCurrent();
            Log.d(TAG,"inputsurface makecurrent");
            outputSurface = new OutputSurface();
            Log.d(TAG,"outputsurface");

        }
        if(fileInd < timelineItems.size()) {
            message = new Message();
            message.arg1 = 'r';
            message.arg2 = fileInd;
            mHandler.sendMessage(message);
            currentTimelineItem = timelineItems.get(fileInd);
            frameIndex = 0;
            generateIndex = 0;
            ratio_width = 1.0f;
            ratio_height = 1.0f;
            widthBigger = 1.0f;
            heightBigger = 1.0f;
            num_frames = 0;

            try {
                //init for video
                if(currentTimelineItem.getIsImage() == false) {
                    Log.d(TAG, "trying to init decoder for file index #" + fileInd);
                    Log.d(TAG, "input file is: " + currentTimelineItem.getOriginalPath());
                    mExtractor = new MediaExtractor();
                    mExtractor.setDataSource(currentTimelineItem.getOriginalPath());


                    for (int i = 0; i < mExtractor.getTrackCount(); i++) {
                        MediaFormat format = mExtractor.getTrackFormat(i);
                        String mime = format.getString(MediaFormat.KEY_MIME);
                        if (mime.startsWith(VIDEO)) {
                            mExtractor.selectTrack(i);

                            try {
                                rotation = format.getInteger(MediaFormat.KEY_ROTATION);
                            } catch (NullPointerException e) {
                                e.printStackTrace();
                                rotation = 0;
                            }
                            if(rotation == 90 || rotation == 270) {
                                originalWidth = format.getInteger(MediaFormat.KEY_HEIGHT);
                                originalHeight = format.getInteger(MediaFormat.KEY_WIDTH);
                            }
                            else {

                                originalWidth = format.getInteger(MediaFormat.KEY_WIDTH);
                                originalHeight = format.getInteger(MediaFormat.KEY_HEIGHT);
                            }

                            //안해도 잘 동작......
                            //if(rotation != 0)
                                //outputSurface.changeOrientation(rotation);
                            outputSurface.changeView(originalWidth, originalHeight,
                                    mWidth, mHeight);

                            mDecoder = MediaCodec.createDecoderByType(mime);
                            try {
                                Log.d(TAG, "format : " + format);
                                mDecoder.configure(format, outputSurface.getSurface(), null, 0); //0:decoder
                            } catch (IllegalStateException e) {
                                Log.e(TAG, "codec '" + mime + "' failed configuration. " + e);
                            }
                            mDecoder.start();
                            float currentDuration = format.getLong(MediaFormat.KEY_DURATION);
                            FRAME_RATE = format.getInteger(MediaFormat.KEY_FRAME_RATE);
                            num_frames = (int)((float)FRAME_RATE * (currentDuration / 1000000));
                            int width = format.getInteger(MediaFormat.KEY_WIDTH);
                            int height = format.getInteger(MediaFormat.KEY_HEIGHT);

                            ratio_height = 1;
                            ratio_width = 1;
                            if (width < height) {
                                heightBigger = 1.0f;
                                widthBigger = 0.0f;
                                ratio_height = (float) width / (float) height;
                            } else {
                                widthBigger = 1.0f;
                                heightBigger = 0.0f;
                                ratio_width = (float) width / (float) height;
                            }
                        }
                    }
                }
                //init for image
                else {
                    float realDuration = currentTimelineItem.getDuration();
                    Log.d(TAG,"duration: "+realDuration);
                    if(currentTimelineItem.getIsSimilarImage())
                        realDuration /= 2.0f;
                    Log.d(TAG,"real_duration: "+realDuration);

                    FRAME_RATE = 30;
                    num_frames = (int)(realDuration/1000.0f *(float)FRAME_RATE);
                    Log.d(TAG,"num_frames: "+num_frames);

                    //step = num_frames*fadeRate*0.5f; //60장 중 10% fade --> 앞 3장 뒤 3장
                    createBitmap(currentTimelineItem.getOriginalPath());
                }
                //step = (float)num_frames*fadeRate*0.5f; //60장 중 10% fade --> 앞 3장 뒤 3장
                //너무 길면 앞뒤 3초만 fade
                step = Math.min((float)num_frames*fadeRate*0.5f, 3*FRAME_RATE);
                Log.d(TAG,"step = "+step);
            } catch (Exception e) {
                e.printStackTrace();
                Log.d(TAG,"init #"+fileInd+" failed!");
                return false;
            }
        }
        return true;
    }

    private void createBitmap(String filePath) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        if(currentTimelineItem.getOrientation() == 90 ||
                currentTimelineItem.getOrientation() == 270) {
            originalWidth = currentTimelineItem.getmHeight();
            originalHeight = currentTimelineItem.getmWidth();
        }
        else {
            originalWidth = currentTimelineItem.getmWidth();
            originalHeight = currentTimelineItem.getmHeight();
        }
        if(originalHeight > mHeight || originalWidth > mWidth) {
            float temp = Math.max((float)originalHeight/(float)mHeight,
                    (float)originalWidth/(float)mHeight);
            options.inSampleSize = (int)Math.max(temp,4);
        } else
            options.inSampleSize = 2;
        //options.inScaled = false;   // No pre-scaling
        //options.inSampleSize=2;
        // Read in the resource
        Log.d(TAG,"original path: "+filePath);
        bitmap = BitmapFactory.decodeFile(filePath, options);
        //bitmap = Bitmap.createScaledBitmap(bitmap,mWidth,mHeight,false);

        Log.d(TAG, "bitmap created path: " + filePath );
        mGLCubeBuffer = ByteBuffer.allocateDirect(CUBE.length * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
        mGLCubeBuffer.put(CUBE).position(0);

        float[] textureBuffer = new float[]{
                0.0f, 1.0f,
                1.0f, 1.0f,
                0.0f, 0.0f,
                1.0f, 0.0f,
        };
        mTextureTransformMatrix = ByteBuffer.allocateDirect(textureBuffer.length * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
        mTextureTransformMatrix.put(textureBuffer).position(0);
    }

    private void drawBitmap(int frameInd) {
        float move = 0.2f/(float)num_frames*(float)frameInd;

        float move_crop = move;

        if(widthBigger == 1.0f)
            move_crop = (1.0f - ratio_width)/(float)num_frames * (float)frameInd;
        else if(heightBigger == 1.0f)
            move_crop = (1.0f - ratio_height)/(float)num_frames * (float)frameInd;

        float[] texture_crop = new float[] {
                0.0f*ratio_width + widthBigger*move_crop, 1.0f*ratio_height + heightBigger*move_crop,
                1.0f*ratio_width + widthBigger*move_crop, 1.0f*ratio_height + heightBigger*move_crop,
                0.0f*ratio_width + widthBigger*move_crop, 0.0f*ratio_height + heightBigger*move_crop,
                1.0f*ratio_width + widthBigger*move_crop, 0.0f*ratio_height + heightBigger*move_crop
        };
        texture_crop = correctOrientation(texture_crop, currentTimelineItem.getOrientation());

        /*
        if(currentTimelineItem.getOrientation() == 90) {
            texture_crop = new float[] {
                    1.0f*ratio_width + widthBigger*move_crop, 1.0f*ratio_height + heightBigger*move_crop,
                    1.0f*ratio_width + widthBigger*move_crop, 0.0f*ratio_height + heightBigger*move_crop,
                    0.0f*ratio_width + widthBigger*move_crop, 1.0f*ratio_height + heightBigger*move_crop,
                    0.0f*ratio_width + widthBigger*move_crop, 0.0f*ratio_height + heightBigger*move_crop
            };
        }
        else if(currentTimelineItem.getOrientation() == 180) {
            texture_crop = new float[] {
                    1.0f*ratio_width + widthBigger*move_crop, 0.0f*ratio_height + heightBigger*move_crop,
                    0.0f*ratio_width + widthBigger*move_crop, 0.0f*ratio_height + heightBigger*move_crop,
                    1.0f*ratio_width + widthBigger*move_crop, 1.0f*ratio_height + heightBigger*move_crop,
                    0.0f*ratio_width + widthBigger*move_crop, 1.0f*ratio_height + heightBigger*move_crop
            };
        }
        else if(currentTimelineItem.getOrientation() == 270) {
            texture_crop = new float[] {
                    0.0f*ratio_width + widthBigger*move_crop, 0.0f*ratio_height + heightBigger*move_crop,
                    0.0f*ratio_width + widthBigger*move_crop, 1.0f*ratio_height + heightBigger*move_crop,
                    1.0f*ratio_width + widthBigger*move_crop, 0.0f*ratio_height + heightBigger*move_crop,
                    1.0f*ratio_width + widthBigger*move_crop, 1.0f*ratio_height + heightBigger*move_crop
            };
        }
        */



        String FADE_FRAGMENT_SHADER = "" +
                "precision mediump float;\n" +
                "varying highp vec2 textureCoordinate;\n" +
                " \n" +
                "uniform sampler2D inputImageTexture;\n" +
                " \n" +
                "void main()\n" +
                "{\n" +
                "     vec4 color = texture2D(inputImageTexture, textureCoordinate);\n" +
                "     float r = " + (1.0f - rate)/2.0f + "+" + (1.0f + rate)/2.0f + "* color.r; \n" +
                "     float g = " + (1.0f - rate)/2.0f + "+" + (1.0f + rate)/2.0f + "* color.g; \n" +
                "     float b = " + (1.0f - rate)/2.0f + "+" + (1.0f + rate)/2.0f + "* color.b; \n" +
                "     gl_FragColor = vec4(r,g,b,1.0); \n" +
                "}";



        mTextureTransformMatrix.put(texture_crop).position(0);

        //if(isFade)
        if((frameInd < step) || (frameInd >= (num_frames - step)))
            changeFragmentShader(true, frameInd);
        //mProgramId = GLUtil.createProgram(NO_FILTER_VERTEX_SHADER, BLACK_FRAGMENT_SHADER);
//        else
//            mProgramId = GLUtil.createProgram(NO_FILTER_VERTEX_SHADER, NO_FILTER_FRAGMENT_SHADER);



        Bitmap bmp = bitmap;
        load(bmp);

        GLES20.glViewport(0, 0, mWidth, mHeight);


        mGLCubeBuffer.position(0);
        GLES20.glVertexAttribPointer(mGLAttribPosition, 2, GLES20.GL_FLOAT, false, 0, mGLCubeBuffer);
        GLES20.glEnableVertexAttribArray(mGLAttribPosition);

        GLES20.glVertexAttribPointer(mGLAttribTextureCoordinate, 2, GLES20.GL_FLOAT, false, 0, mTextureTransformMatrix);
        GLES20.glEnableVertexAttribArray(mGLAttribTextureCoordinate);

        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, imageTextures[0]);
        GLES20.glUniform1i(mGLUniformTexture, 0);

        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, 4);

        GLES20.glDisableVertexAttribArray(mGLAttribPosition);
        GLES20.glDisableVertexAttribArray(mGLAttribTextureCoordinate);

        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);

        //GLES20.glDeleteProgram(mProgramId);
        GLES20.glFinish();


        release();
    }

    float move_horizontal = 0.0f;
    float move_vertical = 0.0f;
    private float highlightRatio=1.0f;

    private void drawBitmap_highlight(int frameInd) {
        if(widthBigger == 1.0f) {
            move_vertical = 0.25f / (float) num_frames * (float) frameInd;
            move_horizontal = 0.25f * highlightRatio / (float) num_frames * (float) frameInd;
        } else {
            move_horizontal = 0.25f / (float) num_frames * (float) frameInd;
            move_vertical = 0.25f * highlightRatio / (float) num_frames * (float) frameInd;
        }

        float[] texture_crop = new float[] {
                (0.5f - 0.25f*highlightRatio)*widthBigger + 0.25f*heightBigger - move_horizontal, 0.75f*widthBigger + (0.5f + 0.25f*highlightRatio)*heightBigger + move_vertical,
                (0.5f + 0.25f*highlightRatio)*widthBigger + 0.75f*heightBigger + move_horizontal, 0.75f*widthBigger + (0.5f + 0.25f*highlightRatio)*heightBigger + move_vertical,
                (0.5f - 0.25f*highlightRatio)*widthBigger + 0.25f*heightBigger - move_horizontal, 0.25f*widthBigger + (0.5f - 0.25f*highlightRatio)*heightBigger - move_vertical,
                (0.5f + 0.25f*highlightRatio)*widthBigger + 0.75f*heightBigger + move_horizontal, 0.25f*widthBigger + (0.5f - 0.25f*highlightRatio)*heightBigger - move_vertical,
        };

        texture_crop = correctOrientation(texture_crop, currentTimelineItem.getOrientation());



        mTextureTransformMatrix.put(texture_crop).position(0);

        if((frameInd < step) || (frameInd >= (num_frames - step)))
            changeFragmentShader(true, frameInd);



        Bitmap bmp = bitmap;
        load(bmp);

        GLES20.glViewport(0, 0, mWidth, mHeight);


        mGLCubeBuffer.position(0);
        GLES20.glVertexAttribPointer(mGLAttribPosition, 2, GLES20.GL_FLOAT, false, 0, mGLCubeBuffer);
        GLES20.glEnableVertexAttribArray(mGLAttribPosition);

        GLES20.glVertexAttribPointer(mGLAttribTextureCoordinate, 2, GLES20.GL_FLOAT, false, 0, mTextureTransformMatrix);
        GLES20.glEnableVertexAttribArray(mGLAttribTextureCoordinate);

        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, imageTextures[0]);
        GLES20.glUniform1i(mGLUniformTexture, 0);

        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, 4);

        GLES20.glDisableVertexAttribArray(mGLAttribPosition);
        GLES20.glDisableVertexAttribArray(mGLAttribTextureCoordinate);

        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);

        GLES20.glDeleteProgram(mProgramId);
        GLES20.glFinish();


        release();
    }

    public void load(Bitmap bitmap) {
        //move to init

        if (null != imageTextures) {
            release();
        }
        imageTextures = new int[1];
        GLES20.glGenTextures(1, imageTextures, 0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, imageTextures[0]);

        GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
        GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);

        GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
        GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);

        GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0);

        int bitmapWidth=1;
        int bitmapHeight=1;

        bitmapWidth = bitmap.getWidth();
        bitmapHeight = bitmap.getHeight();


        ratio_height = 1;
        ratio_width = 1;
        if(bitmapWidth < bitmapHeight) {
            heightBigger = 1.0f;
            widthBigger = 0.0f;
            ratio_height = (float) bitmapWidth / (float) bitmapHeight;
        }
        else {
            widthBigger = 1.0f;
            heightBigger = 0.0f;
            ratio_width = (float) bitmapHeight / (float) bitmapWidth;
        }
        if(currentTimelineItem.getIsHighlight()) {
            if((float)bitmapHeight/(float)bitmapWidth < (float)mHeight/(float)mWidth) {
                highlightRatio = (float)(mWidth*bitmapHeight)/(float)(bitmapWidth*mHeight);
                widthBigger = 1.0f;
                heightBigger = 0.0f;
            }
            else {
                highlightRatio = (float)(bitmapWidth*mHeight)/(float)(mWidth*bitmapHeight);
                heightBigger = 1.0f;
                widthBigger = 0.0f;

            }
        }
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
    }

    public void changeFragmentShader(boolean isImage, int ind) {
        if(VERBOSE) Log.d(TAG,"changeFragmentShader");

        rate_black = 0;
        if(step <= 1.0f)   step = 1.0f;
        //60, 10% --> step = 3;
        if(ind < step) {   //0,1,2
            rate = (float) (ind+1)  / step;
            rate_black=(float)(ind+1)/step*0.9f + 0.1f;
            isFade = true;
        }
        else if(ind >= (num_frames - step)) { //57, 58, 59
            rate = (float)(num_frames - ind)  / step;
            rate_black=(float)(num_frames - ind)/step*0.9f + 0.1f;
            isFade = true;
        }
        else
            isFade=false;

        if(VERBOSE) Log.d(TAG,"rate_black: "+rate_black);
        if(VERBOSE) Log.d(TAG,"step: "+step+", ind = "+ind);


        String BLACK_FRAGMENT_SHADER = "" +
                "precision mediump float;\n" +
                "varying highp vec2 textureCoordinate;\n" +
                " \n" +
                "uniform sampler2D inputImageTexture;\n" +
                " \n" +
                "void main()\n" +
                "{\n" +
                "     vec4 color = texture2D(inputImageTexture, textureCoordinate);\n" +
                "     float r = " + rate_black + "* color.r; \n" +
                "     float g = " + rate_black + "* color.g; \n" +
                "     float b = " + rate_black + "* color.b; \n" +
                "     gl_FragColor = vec4(r,g,b,1.0); \n" +
                "}";

        String FRAGMENT_SHADER =
                "#extension GL_OES_EGL_image_external : require\n" +
                        "precision mediump float;\n" +
                        "varying vec2 vTextureCoord;\n" +
                        "uniform samplerExternalOES sTexture;\n" +
                        "void main() {\n" +
                        "     vec4 color = texture2D(sTexture, vTextureCoord);\n" +
                        "     float r = " + rate_black + "* color.r; \n" +
                        "     float g = " + rate_black + "* color.g; \n" +
                        "     float b = " + rate_black + "* color.b; \n" +
                        "     gl_FragColor = vec4(r,g,b,1.0);\n" +
                        "}\n";
        if(isImage) {
            if (isFade)
                mProgramId = GLUtil.createProgram(NO_FILTER_VERTEX_SHADER, BLACK_FRAGMENT_SHADER);
            else
                mProgramId = GLUtil.createProgram(NO_FILTER_VERTEX_SHADER, NO_FILTER_FRAGMENT_SHADER);

            mGLAttribPosition = GLES20.glGetAttribLocation(mProgramId, "position");
            mGLUniformTexture = GLES20.glGetUniformLocation(mProgramId, "inputImageTexture");
            mGLAttribTextureCoordinate = GLES20.glGetAttribLocation(mProgramId, "inputTextureCoordinate");

            GLES20.glUseProgram(mProgramId);
        }
        else {

            if(isFade)
                outputSurface.changeFragmentShader(FRAGMENT_SHADER);
            else
                outputSurface.changeFragmentShader(VIDEO_FRAGMENT_SHADER);
        }


    }

    public void release() {
        if (imageTextures == null) {
            return;
        }
        try {
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
            GLES20.glDeleteTextures(1, imageTextures, 0);
            //imageTextures = null;

        } catch (Exception e) {
        }
    }

    public float[] correctOrientation(float[] temp, int orientation) {
        float[] result = new float[temp.length];
        if(orientation == 0) {
            return temp;
        }
        else if(orientation == 90) {
            result[0] = temp[2];
            result[1] = temp[3];
            result[2] = temp[6];
            result[3] = temp[7];
            result[4] = temp[0];
            result[5] = temp[1];
            result[6] = temp[4];
            result[7] = temp[5];
        } else if(orientation == 180) {
            result[0] = temp[6];
            result[1] = temp[7];
            result[2] = temp[4];
            result[3] = temp[5];
            result[4] = temp[2];
            result[5] = temp[3];
            result[6] = temp[0];
            result[7] = temp[1];
        }else if(orientation == 270) {
            result[0] = temp[4];
            result[1] = temp[5];
            result[2] = temp[0];
            result[3] = temp[1];
            result[4] = temp[6];
            result[5] = temp[7];
            result[6] = temp[2];
            result[7] = temp[3];
        }

        return result;
    }

    private long computePresentationTime(int frameInd) {
        return 123 + frameInd * 1000000 / FRAME_RATE;
    }

    public void close() {
        eosReceived = true;
        if(th != null) {
            th.interrupt();
        }

    }
}