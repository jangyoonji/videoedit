package com.yoonji.snow.videoedit.mediacodec;

/**
 * Created by snow on 2017-08-06.
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaExtractor;
import android.media.MediaFormat;
import android.media.MediaMuxer;
import android.net.Uri;
import android.opengl.GLES20;
import android.opengl.GLUtils;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.widget.ProgressBar;

import com.yoonji.snow.videoedit.itemview.TimelineItem;
import com.yoonji.snow.videoedit.glutils.GLUtil;
import com.yoonji.snow.videoedit.glutils.InputSurface;
import com.yoonji.snow.videoedit.glutils.OutputSurface;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.ArrayList;

import static junit.framework.Assert.fail;

/**
 * This test has three steps:
 * <ol>
 *   <li>Generate a video test stream.
 *   <li>Decode the video from the stream, rendering frames into a SurfaceTexture.
 *       Render the texture onto a Surface that feeds a video encoder, modifying
 *       the output with a fragment shader.
 *   <li>Decode the second video and compare it to the expected result.
 * </ol><p>
 * The second step is a typical scenario for video editing.  We could do all this in one
 * step, feeding data through multiple stages of MediaCodec, but at some point we're
 * no longer exercising the code in the way we expect it to be used (and the code
 * gets a bit unwieldy).
 */
@RequiresApi(api = Build.VERSION_CODES.M)

public class EncodeSaveThread extends Thread {
    private static final int MAX_SAMPLE_SIZE = 256 * 1024;
    private static final String TAG = "EncodeSaveThread";
    private static final boolean WORK_AROUND_BUGS = false;  // avoid fatal codec bugs
    private static final boolean VERBOSE = false;           // lots of logging

    // parameters for the encoder
    private static final String MIME_TYPE = "video/avc";    // H.264 Advanced Video Coding
    private static int FRAME_RATE = 25;               // 25fps
    private static final int IFRAME_INTERVAL = 5;          // 10 seconds between I-frames


    private TimelineItem currentTimelineItem;
    private MediaExtractor mExtractor;

    private MediaExtractor mBackgroundMusicExtractor;
    private MediaFormat backgroundMusicFormat;
    private int mBackgroundMusicIndex;

    MediaMuxer mMuxer;
    MediaCodec decoder;
    MediaCodec encoder;

    InputSurface inputSurface;
    OutputSurface outputSurface;
    private String output_path;
    boolean mMuxerStarted;
    int update=0;
    private Handler handler;

    private ProgressBar progressBar;

    private int musicID;
    private Uri music_path;
    private Context context;

    double currentDuration = 0.0f;
    // size of a frame, in pixels
    private int mWidth = -1;
    private int mHeight = -1;
    // bit rate, in bits per second
    private int mBitRate = -1;

    private ArrayList<TimelineItem> timelineItems;
    float ratio_width=1.0f;
    float ratio_height=1.0f;
    float widthBigger = 1.0f;
    float heightBigger = 1.0f;
    int num_frames;
    float fadeRate = 0.5f;
    int originalWidth;
    int originalHeight;
    public void init(String outputPath, ArrayList<TimelineItem> timelineitems, int musicId, int width, int height, int bitrate, float faderate, Handler hand, ProgressBar pb, Context context) {
        output_path = outputPath;
        timelineItems = timelineitems;
        musicID = musicId;

        setParameters(width, height, bitrate);
        fadeRate = faderate;
        handler = hand;
        this.context = context;
        progressBar = pb;

        currentDuration = 0.0f;

        //music
        mBackgroundMusicExtractor = new MediaExtractor();
        music_path = Uri.parse("android.resource://"+context.getPackageName()+"/"+musicID);
        try {
            mBackgroundMusicExtractor.setDataSource(context,music_path,null);
            Log.d(TAG,"backgroundMusicExtractor setDataSource");
        } catch (IOException e) {
            e.printStackTrace();
        }

        for(int i=0; i<mBackgroundMusicExtractor.getTrackCount(); i++) {

            backgroundMusicFormat = mBackgroundMusicExtractor.getTrackFormat(i);
            if(backgroundMusicFormat.getString(MediaFormat.KEY_MIME).startsWith("audio/")) {
                mBackgroundMusicExtractor.selectTrack(i);
                Log.d(TAG,"backgroundMusicFormat: " + backgroundMusicFormat);
                break;
            }
        }

    }

    @Override
    public void run() {
        try {
            SaveVideo();
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    //for image
    Bitmap bitmap;

    private float step;
    public static final float CUBE[] = {
            -1.0f, -1.0f,
            1.0f, -1.0f,
            -1.0f, 1.0f,
            1.0f, 1.0f,
    };
    //for drawing image on surface
    private FloatBuffer mGLCubeBuffer;
    private FloatBuffer mTextureTransformMatrix;
    private int mProgramId;

    protected int mGLAttribPosition;
    protected int mGLUniformTexture;
    protected int mGLAttribTextureCoordinate;
    private int[] imageTextures = null;

    public static final String NO_FILTER_VERTEX_SHADER = "" +
            "attribute vec4 position;\n" +
            "attribute vec4 inputTextureCoordinate;\n" +
            " \n" +
            "varying vec2 textureCoordinate;\n" +
            " \n" +
            "void main()\n" +
            "{\n" +
            "    gl_Position = position;\n" +
            "    textureCoordinate = inputTextureCoordinate.xy;\n" +
            "}";
    public static final String NO_FILTER_FRAGMENT_SHADER = "" +
            "varying highp vec2 textureCoordinate;\n" +
            " \n" +
            "uniform sampler2D inputImageTexture;\n" +
            " \n" +
            "void main()\n" +
            "{\n" +
            "     gl_FragColor = texture2D(inputImageTexture, textureCoordinate);\n" +
            "}";


    //for video
    // Replaces TextureRender.FRAGMENT_SHADER during edit; swaps green and blue channels.
    private static final String VIDEO_FRAGMENT_SHADER =
            "#extension GL_OES_EGL_image_external : require\n" +
                    "precision mediump float;\n" +      // highp here doesn't seem to matter
                    "varying vec2 vTextureCoord;\n" +
                    "uniform samplerExternalOES sTexture;\n" +
                    "void main() {\n" +
                    "  gl_FragColor = texture2D(sTexture, vTextureCoord);\n" +
                    "}\n";


    public void SaveVideo() throws Throwable {
        VideoEditWrapper.runTest(this);
    }

    /**
     * Wraps testEditVideo, running it in a new thread.  Required because of the way
     * SurfaceTexture.OnFrameAvailableListener works when the current thread has a Looper
     * configured.
     */
    private static class VideoEditWrapper implements Runnable {
        private Throwable mThrowable;
        private EncodeSaveThread mTest;

        private VideoEditWrapper(EncodeSaveThread test) {
            mTest = test;
        }

        @Override
        public void run() {
            try {
                mTest.videosEditTest();
            } catch (Throwable th) {
                mThrowable = th;
            }
        }

        /** Entry point. */
        public static void runTest(EncodeSaveThread obj) throws Throwable {
            VideoEditWrapper wrapper = new VideoEditWrapper(obj);
            Thread th = new Thread(wrapper, "codec test");
            th.start();
            if (wrapper.mThrowable != null) {
                throw wrapper.mThrowable;
            }
        }
    }

    /**
     * Sets the desired frame size and bit rate.
     */
    private void setParameters(int width, int height, int bitRate) {
        if ((width % 16) != 0 || (height % 16) != 0) {
            Log.w(TAG, "WARNING: width or height not multiple of 16");
        }
        mWidth = width;
        mHeight = height;
        mBitRate = bitRate;
    }


    //for encoding videos to one file
    private void videosEditTest() {
        editVideoFiles();
    }

    private void editVideoFiles() {
        if (VERBOSE) Log.d(TAG, "editVideoFiles " + mWidth + "x" + mHeight);

        decoder = null;
        encoder = null;
        inputSurface = null;
        outputSurface = null;
        mMuxerStarted = false;

        try {
            Log.i(TAG, "Output file is " + output_path);
            mMuxer = new MediaMuxer(output_path, MediaMuxer.OutputFormat.MUXER_OUTPUT_MPEG_4);
            Log.d(TAG, "mMuxer created");
        } catch (IOException e) {
            e.printStackTrace();
        }
        editVideoDatas();

        if (VERBOSE) Log.d(TAG, "shutting down encoder, decoder");

        if (encoder != null) {
            Log.d(TAG,"encoder release start");
            encoder.stop();
            encoder.release();
            Log.d(TAG,"encoder release done");

        }
        /*
        if (decoder != null) {
            Log.d(TAG,"decoder release start");
            decoder.stop();
            decoder.release();
            Log.d(TAG,"decoder release done");

        }
        */
        if (outputSurface != null) {
            Log.d(TAG,"outputSurface release");
            outputSurface.release();
        }
        if (inputSurface != null) {
            Log.d(TAG,"inputSurface release");
            inputSurface.release();
        }
        Log.d(TAG,"mmuxerStarted="+Boolean.toString(mMuxerStarted));
        if(mMuxerStarted == true) {
            mMuxer.stop();
            mMuxer.release();
            Log.d(TAG,"muxer released");
        }
        handler.sendMessage(new Message());
        Log.d(TAG,"everything done");
    }

    int frameIndex=0;
    int rotation = 0;
    public boolean initMediaCodec(int index, boolean isFirstTime) {

        currentTimelineItem = timelineItems.get(index);
        try {
            if (isFirstTime) {
                // Create an encoder format that matches the input format.  (Might be able to just
                // re-use the format used to generate the video, since we want it to be the same.)
                MediaFormat outputFormat = MediaFormat.createVideoFormat(MIME_TYPE, mWidth, mHeight);
                outputFormat.setInteger(MediaFormat.KEY_COLOR_FORMAT,
                        MediaCodecInfo.CodecCapabilities.COLOR_FormatSurface);
                outputFormat.setInteger(MediaFormat.KEY_BIT_RATE, mBitRate);
                outputFormat.setInteger(MediaFormat.KEY_FRAME_RATE, FRAME_RATE);
                outputFormat.setInteger(MediaFormat.KEY_I_FRAME_INTERVAL, IFRAME_INTERVAL);

                encoder = MediaCodec.createEncoderByType(MediaFormat.MIMETYPE_VIDEO_AVC);
                encoder.configure(outputFormat, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);
                inputSurface = new InputSurface(encoder.createInputSurface());
                //inputSurface = new InputSurface(mSurface);
                inputSurface.makeCurrent();
                encoder.start();
                outputSurface = new OutputSurface();
            }

            frameIndex = 0;
            ratio_width = 1.0f;
            ratio_height = 1.0f;
            widthBigger = 1.0f;
            heightBigger = 1.0f;
            num_frames = 0;



            if (currentTimelineItem.getIsImage() == false) {
                /*
                if(decoder != null) {
                    decoder.stop();
                    decoder.release();
                    mExtractor.release();
                }
                */
                mExtractor = new MediaExtractor();
                mExtractor.setDataSource(currentTimelineItem.getOriginalPath());
                for (int i = 0; i < mExtractor.getTrackCount(); i++) {
                    MediaFormat format = mExtractor.getTrackFormat(i);
                    String mime = format.getString(MediaFormat.KEY_MIME);

                    if (mime.startsWith("video/")) {
                        mExtractor.selectTrack(i);
                        currentDuration = format.getLong(MediaFormat.KEY_DURATION);
                        Log.d(TAG, "currentDuration = " + currentDuration);
                        try {
                            Log.d(TAG, "format : " + format);
                            try {
                                rotation = format.getInteger(MediaFormat.KEY_ROTATION);
                            } catch (NullPointerException e) {
                                e.printStackTrace();
                                rotation = 0;
                            }
                            if(rotation == 90 || rotation == 270) {
                                originalWidth = format.getInteger(MediaFormat.KEY_HEIGHT);
                                originalHeight = format.getInteger(MediaFormat.KEY_WIDTH);
                            }
                            else {

                                originalWidth = format.getInteger(MediaFormat.KEY_WIDTH);
                                originalHeight = format.getInteger(MediaFormat.KEY_HEIGHT);
                            }
                            outputSurface.changeView(originalWidth,
                                    originalHeight,
                                    mWidth, mHeight);


                            decoder = MediaCodec.createDecoderByType(mime);
                            decoder.configure(format, outputSurface.getSurface(), null, 0);
                            decoder.start();

                        } catch (IllegalStateException e) {
                            Log.e(TAG, "codec '" + mime + "' failed configuration. " + e);
                        }

                        FRAME_RATE = format.getInteger(MediaFormat.KEY_FRAME_RATE);
                        num_frames = (int)((float)FRAME_RATE * (currentDuration / 1000000));
                        int width = format.getInteger(MediaFormat.KEY_WIDTH);
                        int height = format.getInteger(MediaFormat.KEY_HEIGHT);

                        ratio_height = 1;
                        ratio_width = 1;
                        if (width < height) {
                            heightBigger = 1.0f;
                            widthBigger = 0.0f;
                            ratio_height = (float) width / (float) height;
                        } else {
                            widthBigger = 1.0f;
                            heightBigger = 0.0f;
                            ratio_width = (float) width / (float) height;
                        }
                    }
                }
            }
            else {
                float realDuration = currentTimelineItem.getDuration();
                if(currentTimelineItem.getIsSimilarImage())
                    realDuration /= 2.0f;
                num_frames = (int)(realDuration / 1000.0f *(float)FRAME_RATE);
                //step = num_frames*fadeRate*0.5f; //60장 중 10% fade --> 앞 3장 뒤 3장
                createBitmap(currentTimelineItem.getOriginalPath());
            }
            step = (float)num_frames*fadeRate*0.5f; //60장 중 10% fade --> 앞 3장 뒤 3장
            Log.d(TAG,"step = "+step);
        }catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        Log.d(TAG,"init decoder done");
        return true;
    }

    private void createBitmap(String filePath) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        if(currentTimelineItem.getOrientation() == 90 ||
                currentTimelineItem.getOrientation() == 270) {
            originalWidth = currentTimelineItem.getmHeight();
            originalHeight = currentTimelineItem.getmWidth();
        }
        else {
            originalWidth = currentTimelineItem.getmWidth();
            originalHeight = currentTimelineItem.getmHeight();
        }
        options.inScaled = false;   // No pre-scaling
        // Read in the resource
        Log.d(TAG,"original path: "+filePath);
        bitmap = BitmapFactory.decodeFile(filePath, options);
        Log.d(TAG, "bitmap created path: " + filePath );
        mGLCubeBuffer = ByteBuffer.allocateDirect(CUBE.length * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
        mGLCubeBuffer.put(CUBE).position(0);

        float[] textureBuffer = new float[]{
                0.0f, 1.0f,
                1.0f, 1.0f,
                0.0f, 0.0f,
                1.0f, 0.0f,
        };
        mTextureTransformMatrix = ByteBuffer.allocateDirect(textureBuffer.length * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
        mTextureTransformMatrix.put(textureBuffer).position(0);
    }

    public void editVideoDatas() {
        Log.d(TAG, "start editVideoDatas");
        boolean outputDone = false;
        boolean eosReceived = false;

        int fileInd = 0;
        int mTrackIndex = -1;

        long presentationTimeUsLast = 0;

        while(initMediaCodec(fileInd++,true) == false);

        eosReceived = false;
        isFade = true;
        frameIndex = 0;


        final int TIMEOUT_USEC = 10000;
        ByteBuffer[] encoderOutputBuffers = encoder.getOutputBuffers();
        MediaCodec.BufferInfo info = new MediaCodec.BufferInfo();

        long currentImagePresentationTimeUs = 0;
        int generateIndex = 0;

        if (VERBOSE) Log.d(TAG,"outputDone:"+Boolean.toString(outputDone));

        while (!outputDone ) {
            if (VERBOSE) Log.d(TAG, "edit loop");

            try {
                if (VERBOSE)
                    Log.d(TAG, "fileIndex=" + fileInd + ", eosReceived=" + Boolean.toString(eosReceived));
                if (fileInd < timelineItems.size() && eosReceived == true) {
                    if (VERBOSE) Log.d(TAG, "try to init codec #" + fileInd);
                    if (VERBOSE)
                        Log.d(TAG, "input file path is: " + currentTimelineItem.getOriginalPath());
                    initMediaCodec(fileInd, false);
                    fileInd++;

                    eosReceived = false;
                    generateIndex = 0;
                    isFade = true;
                    frameIndex = 0;
                }

                // Assume output is available.  Loop until both assumptions are false.
                boolean decoderOutputAvailable = !eosReceived;
                boolean encoderOutputAvailable = true;
                if (VERBOSE)
                    Log.d(TAG, "decoderOutputAvailable=" + Boolean.toString(decoderOutputAvailable) +
                            ", encoderOutputAvailable=" + Boolean.toString(encoderOutputAvailable));

                while (decoderOutputAvailable || encoderOutputAvailable) {

                    if (VERBOSE) Log.d(TAG, "while decoder, encoder available");
                    if (VERBOSE)
                        Log.d(TAG, "isImage: " + Boolean.toString(currentTimelineItem.getIsImage()));
                    // Encoder is drained, check to see if we've got a new frame of output from
                    // the decoder.  (The output is going to a Surface, rather than a ByteBuffer,
                    // but we still get information through BufferInfo.)
                    if (!eosReceived && !currentTimelineItem.getIsImage()) {
                        if (VERBOSE) Log.d(TAG, "decoding is video file");

                        int inputIndex = decoder.dequeueInputBuffer(10000);
                        if (inputIndex >= 0) {

                            // fill inputBuffers[inputBufferIndex] with valid data
                            ByteBuffer inputBuffer = decoder.getInputBuffer(inputIndex);
                            int sampleSize = mExtractor.readSampleData(inputBuffer, 0);
                            if (mExtractor.advance() && sampleSize > 0) {
                                decoder.queueInputBuffer(inputIndex, 0, sampleSize, mExtractor.getSampleTime(), 0);
                            } else {
                                if (VERBOSE) Log.d(TAG, "InputBuffer BUFFER_FLAG_END_OF_STREAM");
                                decoder.queueInputBuffer(inputIndex, 0, 0, 0, MediaCodec.BUFFER_FLAG_END_OF_STREAM);
                            }
                        }

                        int decoderStatus = decoder.dequeueOutputBuffer(info, TIMEOUT_USEC);

                        switch (decoderStatus) {
                            case MediaCodec.INFO_TRY_AGAIN_LATER:
                                // no output available yet
                                if (VERBOSE) Log.d(TAG, "no output from decoder available");
                                decoderOutputAvailable = false;
                                break;
                            case MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED:
                                if (VERBOSE)
                                    Log.d(TAG, "decoder output buffers changed (we don't care)");
                                break;
                            case MediaCodec.INFO_OUTPUT_FORMAT_CHANGED:
                                // expected before first buffer of data
                                MediaFormat newFormat = decoder.getOutputFormat();
                                if (VERBOSE)
                                    Log.d(TAG, "decoder output format changed: " + newFormat);
                                break;
                            default: // decoderStatus >= 0
                                if (decoderStatus < 0) {
                                    fail("unexpected result from decoder.dequeueOutputBuffer: " + decoderStatus);
                                } else {
                                    if (VERBOSE) Log.d(TAG, "surface decoder given buffer "
                                            + decoderStatus + " (size=" + info.size + ")");
                                    // The ByteBuffers are null references, but we still get a nonzero
                                    // size for the decoded data.
                                    boolean doRender = (info.size != 0);

                                    // As soon as we call releaseOutputBuffer, the buffer will be forwarded
                                    // to SurfaceTexture to convert to a texture.  The API doesn't
                                    // guarantee that the texture will be available before the call
                                    // returns, so we need to wait for the onFrameAvailable callback to
                                    // fire.  If we don't wait, we risk rendering from the previous frame.
                                    decoder.releaseOutputBuffer(decoderStatus, doRender);

                                    if (doRender) {
                                        // This waits for the image and renders it after it arrives.
                                        if (VERBOSE) Log.d(TAG, "awaiting frame");
                                        if (VERBOSE)
                                            Log.d(TAG, "frameindex = " + frameIndex + ", num_frames = " + num_frames);
                                        if ((frameIndex < step) || (frameIndex >= (num_frames - step)))
                                            changeFragmentShader(false, frameIndex);
                                        //outputSurface.changeFragmentShader(VIDEO_FRAGMENT_SHADER);
                                        frameIndex++;

                                        outputSurface.awaitNewImage();
                                        outputSurface.drawImage();

                                        //frameIndex++;

                                        info.presentationTimeUs += presentationTimeUsLast;
                                        // Send it to the encoder.
                                        inputSurface.setPresentationTime((info.presentationTimeUs) * 1000);
                                        if (VERBOSE) Log.d(TAG, "swapBuffers");
                                        inputSurface.swapBuffers();
                                    }

                                    if ((info.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0) {
                                        // forward decoder EOS to encoder
                                        if (VERBOSE)
                                            Log.d(TAG, "fileIndex = " + fileInd + " , length = " + timelineItems.size());

                                        if (WORK_AROUND_BUGS) {
                                            // Bail early, possibly dropping a frame.
                                            return;
                                        } else {
                                            decoderOutputAvailable = false;
                                            eosReceived = true;
                                            presentationTimeUsLast += info.presentationTimeUs;
                                            if (VERBOSE)
                                                Log.d(TAG, "eosReceived, presentationTimeUsLast; " + presentationTimeUsLast);
                                            if (fileInd == timelineItems.size()) {
                                                if (VERBOSE) Log.d(TAG, "signaling input EOS");
                                                encoder.signalEndOfInputStream();
                                            }
                                            //else {
                                            decoder.stop();
                                            decoder.release();
                                            mExtractor.release();
                                            //}
                                            update = (int) ((float) fileInd / (float) timelineItems.size() * 100);
                                            handler.post(new Runnable() {
                                                @Override
                                                public void run() {
                                                    progressBar.setProgress(update);
                                                }
                                            });
                                        }
                                    }
                                    if ((info.flags & MediaCodec.BUFFER_FLAG_CODEC_CONFIG) != 0) {
                                        info.size = 0;
                                    }
                                }
                                break;
                        }
                    }   //decoding
                    else if (currentTimelineItem.getIsImage() && !eosReceived) {
                        if (VERBOSE) Log.d(TAG, "decoding is image file");
                        if (generateIndex == num_frames) {
                            decoderOutputAvailable = false;
                            eosReceived = true;
                            presentationTimeUsLast = currentImagePresentationTimeUs;
                            if (VERBOSE)
                                Log.d(TAG, "presentationTimeUsLast: " + presentationTimeUsLast);

                            if (fileInd == timelineItems.size()) {
                                // Send an empty frame with the end-of-stream flag set.
                                if (VERBOSE) Log.d(TAG, "signaling input EOS");
                                encoder.signalEndOfInputStream();
                            }
                            update = (int) ((float) fileInd / (float) timelineItems.size() * 100);
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    progressBar.setProgress(update);
                                }
                            });
                        } else {
                            if(currentTimelineItem.getIsHighlight())
                                drawBitmap_highlight(generateIndex);
                            else
                                drawBitmap(generateIndex);
                            currentImagePresentationTimeUs = computePresentationTime(generateIndex);
                            currentImagePresentationTimeUs += presentationTimeUsLast;

                            inputSurface.setPresentationTime(currentImagePresentationTimeUs * 1000);
                            if (VERBOSE) Log.d(TAG, "inputSurface swapBuffers");
                            inputSurface.swapBuffers();
                        }

                        generateIndex++;
                    }

                    // Start by draining any pending output from the encoder.  It's important to
                    // do this before we try to stuff any more data in.
                    int encoderStatus = encoder.dequeueOutputBuffer(info, TIMEOUT_USEC);

                    switch (encoderStatus) {
                        case MediaCodec.INFO_TRY_AGAIN_LATER:
                            // no output available yet
                            if (VERBOSE) Log.d(TAG, "no output from encoder available");
                            encoderOutputAvailable = false;
                            break;
                        case MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED:
                            encoderOutputBuffers = encoder.getOutputBuffers();
                            if (VERBOSE) Log.d(TAG, "encoder output buffers changed");
                            break;
                        case MediaCodec.INFO_OUTPUT_FORMAT_CHANGED:
                            MediaFormat newFormat = encoder.getOutputFormat();
                            if (VERBOSE) Log.d(TAG, "encoder output format changed: " + newFormat);

                            mTrackIndex = mMuxer.addTrack(newFormat);
                            mBackgroundMusicIndex = mMuxer.addTrack(backgroundMusicFormat);
                            mMuxer.start();
                            mMuxerStarted = true;

                            Log.d(TAG, "muxer started");
                            break;

                        default: // encoderStatus >= 0
                            if (encoderStatus < 0) {
                                fail("unexpected result from encoder.dequeueOutputBuffer: " + encoderStatus);
                            } else {
                                ByteBuffer encodedData = encoderOutputBuffers[encoderStatus];

                                if (encodedData == null) {
                                    fail("encoderOutputBuffer " + encoderStatus + " was null");
                                }
                                // Write the data to the output "file".
                                if (info.size != 0) {
                                    encodedData.position(info.offset);
                                    encodedData.limit(info.offset + info.size);

                                    mMuxer.writeSampleData(mTrackIndex, encodedData, info);

                                    if (VERBOSE)
                                        Log.d(TAG, "encoder output " + info.size + " bytes");
                                }
                                outputDone = (info.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0;
                                encoder.releaseOutputBuffer(encoderStatus, false);
                            }
                            break;
                    }
                    if (encoderStatus != MediaCodec.INFO_TRY_AGAIN_LATER) {
                        continue;
                    }

                }
            }catch(Exception e) {
                e.printStackTrace();
                Log.d(TAG,"encoding #"+fileInd+" failed!");
                eosReceived = true;
                if(fileInd == timelineItems.size())
                    outputDone = true;
                continue;
            }
        }

        ByteBuffer backgroundMusicBuf = ByteBuffer.allocate(MAX_SAMPLE_SIZE);
        MediaCodec.BufferInfo backgroundMusicInfo = new MediaCodec.BufferInfo();

        if(VERBOSE) Log.d(TAG,"frame number: "+frameIndex);
        long LastDuration=0;
        while(mMuxerStarted && presentationTimeUsLast > backgroundMusicInfo.presentationTimeUs) {
            backgroundMusicInfo.offset = 0;
            backgroundMusicInfo.size = mBackgroundMusicExtractor.readSampleData(backgroundMusicBuf, 0);
            if (backgroundMusicInfo.size < 0) {
                if (VERBOSE) {
                    Log.d(TAG, "saw input EOS.");
                }
                LastDuration += backgroundMusicFormat.getLong(MediaFormat.KEY_DURATION);
                mBackgroundMusicExtractor = new MediaExtractor();
                try {
                    mBackgroundMusicExtractor.setDataSource(context,music_path,null);
                    Log.d(TAG,"backgroundMusicExtractor setDataSource");
                } catch (IOException e) { e.printStackTrace(); }

                for(int i=0; i<mBackgroundMusicExtractor.getTrackCount(); i++) {
                    backgroundMusicFormat = mBackgroundMusicExtractor.getTrackFormat(i);
                    if(backgroundMusicFormat.getString(MediaFormat.KEY_MIME).startsWith("audio/")) {
                        mBackgroundMusicExtractor.selectTrack(i);
                        Log.d(TAG,"backgroundMusicFormat: " + backgroundMusicFormat);
                        break;
                    }
                }
                backgroundMusicInfo.offset = 0;
                backgroundMusicInfo.size = mBackgroundMusicExtractor.readSampleData(backgroundMusicBuf, 0);
                backgroundMusicInfo.size = 0;
            } else {
                if(VERBOSE) Log.d(TAG, "audio mux");
                backgroundMusicInfo.presentationTimeUs = mBackgroundMusicExtractor.getSampleTime() + LastDuration;
                mMuxer.writeSampleData(mBackgroundMusicIndex, backgroundMusicBuf, backgroundMusicInfo);
                mBackgroundMusicExtractor.advance();
            }
        }
    }

    float rate = 0;
    float rate_black=0;
    boolean isFade = true;

    private void drawBitmap(int frameInd) {
        float move = 0.2f/(float)num_frames*(float)frameInd;

        float move_crop = move;

        if(widthBigger == 1.0f)
            move_crop = (1.0f - ratio_width)/(float)num_frames * (float)frameInd;
        else if(heightBigger == 1.0f)
            move_crop = (1.0f - ratio_height)/(float)num_frames * (float)frameInd;

        float[] texture_crop = new float[] {
                0.0f*ratio_width + widthBigger*move_crop, 1.0f*ratio_height + heightBigger*move_crop,
                1.0f*ratio_width + widthBigger*move_crop, 1.0f*ratio_height + heightBigger*move_crop,
                0.0f*ratio_width + widthBigger*move_crop, 0.0f*ratio_height + heightBigger*move_crop,
                1.0f*ratio_width + widthBigger*move_crop, 0.0f*ratio_height + heightBigger*move_crop
        };
        if(currentTimelineItem.getOrientation() == 90) {
            texture_crop = new float[] {
                    1.0f*ratio_width + widthBigger*move_crop, 1.0f*ratio_height + heightBigger*move_crop,
                    1.0f*ratio_width + widthBigger*move_crop, 0.0f*ratio_height + heightBigger*move_crop,
                    0.0f*ratio_width + widthBigger*move_crop, 1.0f*ratio_height + heightBigger*move_crop,
                    0.0f*ratio_width + widthBigger*move_crop, 0.0f*ratio_height + heightBigger*move_crop
            };
        }
        else if(currentTimelineItem.getOrientation() == 180) {
            texture_crop = new float[] {
                    1.0f*ratio_width + widthBigger*move_crop, 0.0f*ratio_height + heightBigger*move_crop,
                    0.0f*ratio_width + widthBigger*move_crop, 0.0f*ratio_height + heightBigger*move_crop,
                    1.0f*ratio_width + widthBigger*move_crop, 1.0f*ratio_height + heightBigger*move_crop,
                    0.0f*ratio_width + widthBigger*move_crop, 1.0f*ratio_height + heightBigger*move_crop
            };
        }
        else if(currentTimelineItem.getOrientation() == 270) {
            texture_crop = new float[] {
                    0.0f*ratio_width + widthBigger*move_crop, 0.0f*ratio_height + heightBigger*move_crop,
                    0.0f*ratio_width + widthBigger*move_crop, 1.0f*ratio_height + heightBigger*move_crop,
                    1.0f*ratio_width + widthBigger*move_crop, 0.0f*ratio_height + heightBigger*move_crop,
                    1.0f*ratio_width + widthBigger*move_crop, 1.0f*ratio_height + heightBigger*move_crop
            };
        }


        String FADE_FRAGMENT_SHADER = "" +
                "precision mediump float;\n" +
                "varying highp vec2 textureCoordinate;\n" +
                " \n" +
                "uniform sampler2D inputImageTexture;\n" +
                " \n" +
                "void main()\n" +
                "{\n" +
                "     vec4 color = texture2D(inputImageTexture, textureCoordinate);\n" +
                "     float r = " + (1.0f - rate)/2.0f + "+" + (1.0f + rate)/2.0f + "* color.r; \n" +
                "     float g = " + (1.0f - rate)/2.0f + "+" + (1.0f + rate)/2.0f + "* color.g; \n" +
                "     float b = " + (1.0f - rate)/2.0f + "+" + (1.0f + rate)/2.0f + "* color.b; \n" +
                "     gl_FragColor = vec4(r,g,b,1.0); \n" +
                "}";



        mTextureTransformMatrix.put(texture_crop).position(0);

        //if(isFade)
        if((frameInd < step) || (frameInd >= (num_frames - step)))
            changeFragmentShader(true, frameInd);
            //mProgramId = GLUtil.createProgram(NO_FILTER_VERTEX_SHADER, BLACK_FRAGMENT_SHADER);
//        else
//            mProgramId = GLUtil.createProgram(NO_FILTER_VERTEX_SHADER, NO_FILTER_FRAGMENT_SHADER);



        Bitmap bmp = bitmap;
        load(bmp);

        GLES20.glViewport(0, 0, mWidth, mHeight);


        mGLCubeBuffer.position(0);
        GLES20.glVertexAttribPointer(mGLAttribPosition, 2, GLES20.GL_FLOAT, false, 0, mGLCubeBuffer);
        GLES20.glEnableVertexAttribArray(mGLAttribPosition);

        GLES20.glVertexAttribPointer(mGLAttribTextureCoordinate, 2, GLES20.GL_FLOAT, false, 0, mTextureTransformMatrix);
        GLES20.glEnableVertexAttribArray(mGLAttribTextureCoordinate);

        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, imageTextures[0]);
        GLES20.glUniform1i(mGLUniformTexture, 0);

        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, 4);

        GLES20.glDisableVertexAttribArray(mGLAttribPosition);
        GLES20.glDisableVertexAttribArray(mGLAttribTextureCoordinate);

        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);

        //GLES20.glDeleteProgram(mProgramId);
        GLES20.glFinish();


        release();
    }

    float move_horizontal = 0.0f;
    float move_vertical = 0.0f;
    private float highlightRatio=1.0f;
    private void drawBitmap_highlight(int frameInd) {
        if(widthBigger == 1.0f) {
            move_vertical = 0.25f / (float) num_frames * (float) frameInd;
            move_horizontal = 0.25f * highlightRatio / (float) num_frames * (float) frameInd;
        } else {
            move_horizontal = 0.25f / (float) num_frames * (float) frameInd;
            move_vertical = 0.25f * highlightRatio / (float) num_frames * (float) frameInd;
        }

        float[] texture_crop = new float[] {
                (0.5f - 0.25f*highlightRatio)*widthBigger + 0.25f*heightBigger - move_horizontal, 0.75f*widthBigger + (0.5f + 0.25f*highlightRatio)*heightBigger + move_vertical,
                (0.5f + 0.25f*highlightRatio)*widthBigger + 0.75f*heightBigger + move_horizontal, 0.75f*widthBigger + (0.5f + 0.25f*highlightRatio)*heightBigger + move_vertical,
                (0.5f - 0.25f*highlightRatio)*widthBigger + 0.25f*heightBigger - move_horizontal, 0.25f*widthBigger + (0.5f - 0.25f*highlightRatio)*heightBigger - move_vertical,
                (0.5f + 0.25f*highlightRatio)*widthBigger + 0.75f*heightBigger + move_horizontal, 0.25f*widthBigger + (0.5f - 0.25f*highlightRatio)*heightBigger - move_vertical,
        };

        texture_crop = correctOrientation(texture_crop, currentTimelineItem.getOrientation());



        mTextureTransformMatrix.put(texture_crop).position(0);

        if((frameInd < step) || (frameInd >= (num_frames - step)))
            changeFragmentShader(true, frameInd);



        Bitmap bmp = bitmap;
        load(bmp);

        GLES20.glViewport(0, 0, mWidth, mHeight);


        mGLCubeBuffer.position(0);
        GLES20.glVertexAttribPointer(mGLAttribPosition, 2, GLES20.GL_FLOAT, false, 0, mGLCubeBuffer);
        GLES20.glEnableVertexAttribArray(mGLAttribPosition);

        GLES20.glVertexAttribPointer(mGLAttribTextureCoordinate, 2, GLES20.GL_FLOAT, false, 0, mTextureTransformMatrix);
        GLES20.glEnableVertexAttribArray(mGLAttribTextureCoordinate);

        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, imageTextures[0]);
        GLES20.glUniform1i(mGLUniformTexture, 0);

        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, 4);

        GLES20.glDisableVertexAttribArray(mGLAttribPosition);
        GLES20.glDisableVertexAttribArray(mGLAttribTextureCoordinate);

        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);

        GLES20.glDeleteProgram(mProgramId);
        GLES20.glFinish();


        release();
    }

    public void load(Bitmap bitmap) {
        //move to init

        if (null != imageTextures) {
            release();
        }
        imageTextures = new int[1];
        GLES20.glGenTextures(1, imageTextures, 0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, imageTextures[0]);

        GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
        GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);

        GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
        GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);

        GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0);

        int bitmapWidth = bitmap.getWidth();
        int bitmapHeight = bitmap.getHeight();

        ratio_height = 1;
        ratio_width = 1;
        if(bitmapWidth < bitmapHeight) {
            heightBigger = 1.0f;
            widthBigger = 0.0f;
            ratio_height = (float) bitmapWidth / (float) bitmapHeight;
        }
        else {
            widthBigger = 1.0f;
            heightBigger = 0.0f;
            ratio_width = (float) bitmapHeight / (float) bitmapWidth;
        }
        //Log.d(TAG,"bitmapWidth = " + bitmapWidth + "bitmapHeight = " +bitmapHeight);

        if(currentTimelineItem.getIsHighlight()) {
            if((float)bitmapHeight/(float)bitmapWidth < (float)mHeight/(float)mWidth) {
                highlightRatio = (float)(mWidth*bitmapHeight)/(float)(bitmapWidth*mHeight);
                widthBigger = 1.0f;
                heightBigger = 0.0f;
            }
            else {
                highlightRatio = (float)(bitmapWidth*mHeight)/(float)(mWidth*bitmapHeight);
                heightBigger = 1.0f;
                widthBigger = 0.0f;

            }
        }
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
    }

    public float[] correctOrientation(float[] temp, int orientation) {
        float[] result = new float[temp.length];
        if(orientation == 0) {
            return temp;
        }
        else if(orientation == 90) {
            result[0] = temp[2];
            result[1] = temp[3];
            result[2] = temp[6];
            result[3] = temp[7];
            result[4] = temp[0];
            result[5] = temp[1];
            result[6] = temp[4];
            result[7] = temp[5];
        } else if(orientation == 180) {
            result[0] = temp[6];
            result[1] = temp[7];
            result[2] = temp[4];
            result[3] = temp[5];
            result[4] = temp[2];
            result[5] = temp[3];
            result[6] = temp[0];
            result[7] = temp[1];
        }else if(orientation == 270) {
            result[0] = temp[4];
            result[1] = temp[5];
            result[2] = temp[0];
            result[3] = temp[1];
            result[4] = temp[6];
            result[5] = temp[7];
            result[6] = temp[2];
            result[7] = temp[3];
        }

        return result;
    }

    public void changeFragmentShader(boolean isImage, int ind) {
        if(VERBOSE) Log.d(TAG,"changeFragmentShader");

        rate_black = 0;
        if(step <= 1.0f)   step = 1.0f;
        //60, 10% --> step = 3;
        if(ind < step) {   //0,1,2
            rate = (float) (ind+1)  / step;
            rate_black=(float)(ind+1)/step*0.8f + 0.2f;
            isFade = true;
            //Log.d(TAG,"frameInd: "+frameInd+", fade rate: "+(1.0f-rate)*100+"%");
        }
        else if(ind >= (num_frames - step)) { //57, 58, 59
            rate = (float)(num_frames - ind)  / step;
            rate_black=(float)(num_frames - ind)/step*0.8f + 0.2f;
            isFade = true;
            //Log.d(TAG,"frameInd: "+frameInd+", fade rate: "+(1.0f-rate)*100+"%");
        }
        else
            isFade=false;

        if(VERBOSE) Log.d(TAG,"rate_black: "+rate_black);
        if(VERBOSE) Log.d(TAG,"step: "+step+", ind = "+ind);


        String BLACK_FRAGMENT_SHADER = "" +
                "precision mediump float;\n" +
                "varying highp vec2 textureCoordinate;\n" +
                " \n" +
                "uniform sampler2D inputImageTexture;\n" +
                " \n" +
                "void main()\n" +
                "{\n" +
                "     vec4 color = texture2D(inputImageTexture, textureCoordinate);\n" +
                "     float r = " + rate_black + "* color.r; \n" +
                "     float g = " + rate_black + "* color.g; \n" +
                "     float b = " + rate_black + "* color.b; \n" +
                "     gl_FragColor = vec4(r,g,b,1.0); \n" +
                "}";

        String FRAGMENT_SHADER =
                "#extension GL_OES_EGL_image_external : require\n" +
                        "precision mediump float;\n" +
                        "varying vec2 vTextureCoord;\n" +
                        "uniform samplerExternalOES sTexture;\n" +
                        "void main() {\n" +
                        "     vec4 color = texture2D(sTexture, vTextureCoord);\n" +
                        "     float r = " + rate_black + "* color.r; \n" +
                        "     float g = " + rate_black + "* color.g; \n" +
                        "     float b = " + rate_black + "* color.b; \n" +
                        "     gl_FragColor = vec4(r,g,b,1.0);\n" +
                        "}\n";
        if(isImage) {
            if (isFade)
                mProgramId = GLUtil.createProgram(NO_FILTER_VERTEX_SHADER, BLACK_FRAGMENT_SHADER);
            else
                mProgramId = GLUtil.createProgram(NO_FILTER_VERTEX_SHADER, NO_FILTER_FRAGMENT_SHADER);

            mGLAttribPosition = GLES20.glGetAttribLocation(mProgramId, "position");
            mGLUniformTexture = GLES20.glGetUniformLocation(mProgramId, "inputImageTexture");
            mGLAttribTextureCoordinate = GLES20.glGetAttribLocation(mProgramId, "inputTextureCoordinate");

            GLES20.glUseProgram(mProgramId);
        }
        else {

            if(isFade)
                outputSurface.changeFragmentShader(FRAGMENT_SHADER);
            else
                outputSurface.changeFragmentShader(VIDEO_FRAGMENT_SHADER);
        }


    }
    public void release() {
        if (imageTextures == null) {
            return;
        }
        try {
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
            GLES20.glDeleteTextures(1, imageTextures, 0);
            //imageTextures = null;

        } catch (Exception e) {
        }
    }

    private static long computePresentationTime(int frameInd) {
        return 123 + frameInd * 1000000 / FRAME_RATE;
    }
}

