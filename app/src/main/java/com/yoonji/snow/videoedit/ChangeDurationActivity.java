package com.yoonji.snow.videoedit;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.yoonji.snow.videoedit.itemview.TimelineItem;

import java.io.File;

/**
 * Created by snow on 2017-08-04.
 */
@RequiresApi(api = Build.VERSION_CODES.M)

public class ChangeDurationActivity extends Activity {
    private ImageView imageView;
    private SeekBar seekBar;
    private Button okButton;
    private TextView textView;

    private TimelineItem timelineItem;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editfile );

        imageView = (ImageView) findViewById(R.id.imageView);
        seekBar = (SeekBar) findViewById(R.id.seekBar);
        okButton = (Button) findViewById(R.id.okButton);
        textView = (TextView) findViewById(R.id.textView);

        imageView.setScaleType(ImageView.ScaleType.FIT_XY);

        Bundle bundle = getIntent().getExtras();
        timelineItem = bundle.getParcelable("pickedItem");

        seekBar.setProgress((int)((float)timelineItem.getDuration()/(float)1000*(float)2));
        textView.setText((float)seekBar.getProgress()/2+"s");

        Glide.with(getApplicationContext())
                .load(new File(timelineItem.getOriginalPath()))
                .thumbnail(0.1f)
                .into(imageView);
        //imageView.setImageBitmap(timelineItem.getBitmap());

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                textView.setText((float)i/2+"s");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(timelineItem.getIsImage()) {
                    Intent intent = new Intent();
                    intent.putExtra("result",(long) seekBar.getProgress() * 1000 /2);
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                }
                finish();
            }
        });
    }
}
